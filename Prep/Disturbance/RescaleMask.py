# This script rescales a vegetation mask from 30 meter pixels to 250 meter pixels
# The upscaled pixels in the output mask range from 0 (no mask) to 1 masked. 
# Intermediate values reflext the amount of the 250 meter pixel that was covered by masked cells 
# A simple reproject does not work instead reprojection is done to 25 meter pixels with snapping from the 250 m grid
# then the grid is clipped to the extent of the 250 meter grid 
# next the block statistics (mean) are calcualted on 10 cell square blocks 
# finally nearest neighbor upscaling is done to get to the 250 m cells.
#
#  Ethan Plunkett
#   Oct 18 2011

try:
    hirespath =  "W:/LCC/GISdata/DataWorking/Vegetation/kvegmask30"
    extentpath = "W:/LCC/GISdata/DataWorking/Vegetation/FIAimputations/kennage250"
    outpatha = "W:/LCC/GISdata/DataWorking/Vegetation/kvegmask25"  # the resampled image at a 25 m resolution
    outpathb = "W:/LCC/GISdata/DataWorking/Vegetation/kvegmask25b" # the clipped image
    outpathc = "W:/LCC/GISdata/DataWorking/Vegetation/kvegmask25c" # with block statistics
    outpathd = "W:/LCC/GISdata/DataWorking/Vegetation/kvegmask250"
    import arcpy
    lccprojection = "Y:/LCC/GISdata/PrjFiles/NAD83Albers.prj"

    # Set Snap Raster environment
    arcpy.env.snapRaster = extentpath
    arcpy.env.workspace = "C:/Workspace"

    # Delete output grids (so we can overwrite them)
    if arcpy.Exists(outpatha):
        arcpy.Delete_management(outpatha, "")
    if arcpy.Exists(outpathb):
        arcpy.Delete_management(outpathb, "")
    if arcpy.Exists(outpathc):
        arcpy.Delete_management(outpathc, "")
    if arcpy.Exists(outpathd):
        arcpy.Delete_management(outpathd, "")
        
    # Define project of the recently created 30m vegetation mask
    arcpy.DefineProjection_management(hirespath, lccprojection)    
    
    # Reproject to 25 meter cells                           
    arcpy.ProjectRaster_management(hirespath,\
                               outpatha,\
                               hirespath, "BILINEAR", "25",\
                               "#", "#", "#")
# Arguments to ProjectRaster_management
# 1. inraster
# 2. outraster
# 3. out_coor_system
# 4. resampling type (optional)
# 5. cellsize (optional)
# 6. geographic transform (optional)
# 7. registration point (optional)
# 8. incoordinate system (optional)

    arcpy.Clip_management(outpatha, "#",\
                  outpathb, extentpath, "#", "NONE")
# Arguments to Clip_management
# 1. inraster
# 2. rectangle - leave out ("#") if using the extent of another file
# 3. outraster
# 4. in template dataset (dataset to use for extent)
# 5. no data value (optional)
# 6. clipping geometry (optional)

    from arcpy import env
    from arcpy.sa import *
    arcpy.CheckOutExtension("Spatial")
    nbr = NbrRectangle(10, 10, "CELL") 
    outBlockStat = BlockStatistics(outpathb, nbr, "MEAN", "NODATA")
    # Save the output 
    outBlockStat.save(outpathc)
# Arguments to BlockStatistics
# 1. in_raster
# 2. neighborhood (optional)
# 3. statistics_type
# 4. ignore_nodata

    arcpy.DefineProjection_management(outpathc, lccprojection)    
    arcpy.Resample_management(outpathc,outpathd,"250","NEAREST")

#    arcpy.ProjectRaster_management(outpathc,\
#                               outpathd,\
#                               extentpath, "NEAREST", "250",\
#                               "#", "#", "#")

#   Delete the intermediate grids
    arcpy.Delete_management(outpatha, "")
    arcpy.Delete_management(outpathb, "")
    arcpy.Delete_management(outpathc, "")


except:
    print "RescaleMask Failed"
    print arcpy.GetMessages()
