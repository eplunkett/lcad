# Note I'm renaming the cr.expand column heading to cr.shift which is, I think more acurate!!!



summary <- "kenn"

run.types <- c("base", "focal", "secure")
run.type <-"base" #  "base" , "focal", or "secure"

#for(run.type in run.types){

all.species <- unique(p("species.info")$species)
if(summary == "pocnan") all.species <- c("woth", "mawr", "rsha", "bhnu","oven", "lowa")
switch(run.type, 
       focal = {
         rdata.file <- "focal.habpost.Rdata"
         focal <- TRUE
       },
       secure = {
         rdata.file <- "secure.habpost.Rdata"
       },
       base ={
         rdata.file <- "habpost.Rdata" 
       })


timesteps <- c(2, 7)

# Setup

s <- lookup.summary(summary)
set.scenario(s$scen[1])  # these two lines are so g works.  We are looking up stuff that is common to all the scenarios
set.rep(s$rep[1])

hectares.per.cell <- p("cellsize")^2 / 10000

out.dir <- paste(g("base", 0), "/Output/summaries/", summary, "/", sep="")



# Make empty data structures
arr <- asr <-NULL
raw.rep.data  <- NULL 
raw.spp.data  <- NULL
stand <- NULL # standardized data


species.has.data <- matrix(FALSE, nrow=length(all.species), ncol=length(timesteps), dimnames=list(all.species, timesteps))
mtime <- matrix(NA, nrow=length(all.species), ncol=length(timesteps), dimnames=list(all.species, timesteps))

coa.standardized.cols <- c("persist", "contract", "expand")
lc.standardized.cols <-  c("cr.none", "cr.contract", "cr.shift")

for(i in 1:length(timesteps)){
  # i <- 1
  timestep <- timesteps[i]
  for(j in 1:length(all.species)){
    # j <- 2
    species <- all.species[j]
    
    spp.dir <-  paste(g("base",0), "/Output/summaries/", summary, "/",pad(timestep, 2), "/", species, "/", sep="")  
    file.path <- paste(spp.dir, rdata.file, sep="")
    if(!file.exists(file.path)) next  # can't aggregate data that isn't there
    species.has.data[j, i] <- TRUE  
    mtime[j, i] <- as.character(file.info(file.path)$mtime)
    load(file.path)
    
    # Fix names    
    names(rr)[names(rr) == "cr.expand"] <- "cr.shift"
    
    # append to raw data tables
    raw.rep.data <-rbind(raw.rep.data, rr)
    raw.spp.data  <-rbind(raw.spp.data, sr)
    
    
    # Aggregate rr so that it has three rows one each for the mean, max, and min of each metric
    rr <- rr[, !names(rr) %in% c("scen", "rep", "sres")]
    rrmax <- rrmin <- rrmean <- rr[1, , drop=FALSE]
    numcols <- rep(FALSE, ncol(rr))  
    for(k in 1:ncol(rr)) if(is.numeric(rr[, k])) numcols[k] <- TRUE
    rrmax[1, numcols] <- apply(rr[, numcols], 2, max)
    rrmin[1, numcols] <- apply(rr[, numcols], 2, min)
    rrmean[1, numcols] <- apply(rr[, numcols], 2, mean)
    rrmax$statistic <- "max"
    rrmin$statistic <- "min"
    rrmean$statistic <- "mean"
    rr2 <- rbind(  rrmean,rrmin, rrmax)
    
    # save aggregated version
    arr<- rbind(arr, rr2)
    asr <- rbind(asr, sr)
    
    # standardized version
    rr3 <- rr2
    sv <- colnames(rr3) %in% coa.standardized.cols
    rr3[, sv] <- rr2[, sv] / sr$coa
    sv <- colnames(rr3) %in% lc.standardized.cols
    rr3[, sv] <- rr2[, sv] / sr$lc
    stand <- rbind(stand, rr3)
  
    
  }
}


write.csv(raw.rep.data, paste(out.dir, ifelse(run.type=="base","", paste(run.type, "." ,sep="" )), "raw.rep.data.csv", sep="" ), row.names=FALSE)
write.csv(raw.spp.data, paste(out.dir,ifelse(run.type=="base","", paste(run.type, ".", sep="" )), "raw.spp.data.csv", sep="" ), row.names=FALSE)



#} # End run types loop

# Make a backup (just so we can rerun from here with a clean copy of stand if we need to)
ostand <- stand

stand <- ostand


library(reshape)

# Add year column
years <- seq(p("startyear"), p("startyear") + p("stepsize")*p("nsteps") , p("stepsize"))
stand$year<-as.factor(years[match(stand$timestep, 0:p("nsteps"))])


### Table 2
t2cols <- c( "cne", "species","year",  "statistic")
a <- stand[ , colnames(stand) %in% t2cols]
a$cne <- a$cne * hectares.per.cell
a <- melt(a)
names(a)
a <- cast(a, formula= species + statistic ~  variable + year )
a$cne_2010 <- asr$cne[match(a$species, asr$species)]*hectares.per.cell
a$prop_2030 <- (a$cne_2030 - a$cne_2010)/a$cne_2010
a$prop_2080 <- (a$cne_2080 - a$cne_2010)/a$cne_2010
a <- a[, match( c("species", "statistic", "cne_2010", "cne_2030", "prop_2030", "cne_2080", "prop_2080"), colnames(a))]
# Sort so statistics are in the order "mean", "min", "max"
temp <- match(a$statistic, c("mean", "min", "max"))
a <- a[order(a$species, temp), ]
a$cne_2010[rep(c(FALSE, TRUE, TRUE), length.out=nrow(a))] <- NA
a$species[rep(c(FALSE, TRUE, TRUE), length.out=nrow(a))] <- NA
#Reformat columns
a$species <- as.character(a$species)
a$species[is.na(a$species)] <- ""
# reformat cne columns (round off decimal places and then add big marks)
cne.cols <- grep("cne", colnames(a))
a[, cne.cols] <- format(round(a[, cne.cols], digits=0), big.mark=",", nsmall=0)
# reformat proportion columns 
prop.cols <- grep("prop", colnames(a))
a[, prop.cols] <- format(round(a[, prop.cols], 2), digits=2)

# Clear NA text in cne.cols
for(col in cne.cols) a[, col] <- gsub("[[:blank:]]*NA[[:blank:]]*", "", a[, col])

# add header rows
header1 <- gsub("[^[:digit:]]*", "", colnames(a))
header1[duplicated(header1)] <- ""
header2 <- rep("", ncol(a))
header2[grep("cne", names(a))] <- "Area (ha)"
header2[grep("prop", names(a))] <- "Change"
sv <- header2 == ""
header2[sv] <- gsub("^(.)", "\\U\\1", names(a)[sv], perl=TRUE)
a <- rbind(header1, header2,  a)

for(i in 1:ncol(a)){
  a[, i] <- as.character(a[,i])
  if(any(is.na(a[, i]))) a[is.na(a[, i]), i] <- ""
  sv <- grep("NA", a[, i])
  if(length(sv) > 0) a[sv, i] <- ""
}

write.table(a, paste(out.dir, "table2.txt",sep=""), row.names=FALSE, col.names=FALSE, sep="\t", na="")


### Table 3
t3cols <- c("cr.none","cr.contract", "cr.shift", "species","year",  "statistic")
a <- stand[ , colnames(stand) %in% t3cols]
a <- melt(a)
a <- cast(a, formula= species + statistic ~  variable + year )

a$lc <- asr$lc[match(a$species, asr$species)] *hectares.per.cell
a <- a[, match( c("species",  "statistic","lc", "cr.none_2030", "cr.none_2080", "cr.contract_2030", "cr.contract_2080", "cr.shift_2030", "cr.shift_2080"), colnames(a))]

# Sort so statistics are in the order "mean", "min", "max"
temp <- match(a$statistic, c("mean", "min", "max"))
a <- a[order(a$species, temp), ]

a$lc[rep(c(FALSE, TRUE, TRUE), length.out=nrow(a))] <- NA
a$species[rep(c(FALSE, TRUE, TRUE), length.out=nrow(a))] <- NA

a$lc <- format(round(a$lc, 0), big.mark=",")
sv <- grep("^cr", names(a))
a[, sv] <- format(a[ , sv], digits=2, nsmall=2)


for(i in 1:ncol(a)){
  a[, i] <- as.character(a[,i])
  if(any(is.na(a[, i]))) a[is.na(a[, i]), i] <- ""
  sv <- grep("NA", a[, i])
  if(length(sv) > 0 ) a[sv, i] <- ""
}

header1 <- header2 <- header3 <- header4 <-  rep("", ncol(a))
header1[  4] <- "Species Response to Climate Change"

header2[ c(4, 6, 8)] <- c("None", "Immediate Range Contraction", "Immediate Range Shift")

sv <- grep("[[:digit:]]{4}", names(a))
header4 <- gsub(".*([[:digit:]]{4}).*", "\\1", names(a))

header4 <- gsub("(^.)(.*)", "\\U\\1\\L\\2", header4, perl=TRUE)

lci <- names(a) =="lc"
header3[lci] <-get.year(timestep=0, pad=4)
header4[lci] <- "(HRC x HA)"

b <- rbind(header1, header2, header3, header4, a)


write.table(b, paste(out.dir, "table3.txt",sep=""), row.names=FALSE, col.names=FALSE, na="", sep="\t")


###### Table 4
rm(a)

t4cols <- c("persist", "contract", "expand", "species","year",  "statistic")
a <- stand[ , colnames(stand) %in% t4cols]
a <- melt(a)

a <- cast(a, formula= species + statistic ~  variable + year )
# a is now the right part of the table

a$coa <- asr$coa[match(a$species, asr$species)] *hectares.per.cell

temp <- match(a$statistic, c("mean", "min", "max"))
a <- a[order(a$species, temp), ]

# Order columns
desired.order <-c("species", "statistic", "coa", "persist_2030","persist_2080",
                  "contract_2030", "contract_2080", "expand_2030", "expand_2080")

if(!all(desired.order %in% names(a))) stop("Missing an important column in table 4")
a <- a[, match(desired.order, names(a))]

# Reformat
a$species <- as.character(a$species)
a$species[rep(c(FALSE, TRUE, TRUE),length.out=nrow(a))] <- ""
a$coa <- format(round(a$coa, 0), big.mark="," )
a$coa[rep(c(FALSE, TRUE, TRUE),length.out=nrow(a))] <- ""# Delete repeated values in coa
sv <- grep("[[:digit:]]{4}", names(a)) # date index
digits <- 2
a[, sv] <-  format(round(a[ , sv], digits=digits), digits=digits, nsmall=digits) # round al the proportions to 2 digits


header1 <- header2 <- header3 <- rep("", ncol(a))
for(n in c("Persist", "Contract", "Expand")){
  # n="Persist"
  b <- grep(n, names(a),ignore.case=TRUE )
  header1[b[1]] <- n
}
header2[names(a)=="coa"] <- "COA"
sv <- grep("[[:digit:]]{4}", names(a)) # date index
header3 <- gsub(".*([[:digit:]]{4}).*", "\\1", names(a))
header3[names(a) == "coa"] <- "(ha)"

a <- rbind(header1, header2, header3, a)

write.table(a, paste(out.dir, "table4.txt",sep=""), row.names=FALSE,col.names=FALSE, sep="\t", na="" )


#


