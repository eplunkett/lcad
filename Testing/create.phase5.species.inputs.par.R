

# Create inputs_phase5.par for each species in the original directory
# This is for updating inputs.par  to phase5 and for validating input paths

rm(list = ls())
source("X:/LCC/LCAD/load.LCAD.R")
si <- p("species.info")
si  <-  si[!si$species == "dev", ]

i <- 1
i <- 19  # Oven  bird
i <- 24


for(i in i:nrow(si)){
  sp <- si$species[i]
  source <- paste0(g("habitatpar", species = sp), "inputs.par")
  dest <- paste0(g("habitatpar", species = sp), "inputs_phase5.par")
  file.copy(source, dest, overwrite = TRUE)
  # Update inputs.par -- changes file on disk and returns a dataframe with "alias" and "path" columns
  inputs <- update.habitat.inputs(file= dest, check.static.grids = TRUE)
}



