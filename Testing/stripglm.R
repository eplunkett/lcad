
# In this script I hack around with a glm object to see how much I can throw out and still 
#  call predict successfully.



# set working directory.
setwd('Y:/LCC/GISData/DataWorking/Anthropogenic/Conn_urban_growth/double')  # CLUSTER

# load the logistic regresion fits
load('training_data/LR.fits.Rdata')

# Extract a single glm fit object from the list of lists

# f is a glm fit for a particular transition and tile
f <- LR.fits[[32]][[6]]
n <- length(f$residuals)
sv <- rep(FALSE, length(names(f)))
for(i in 1:length(names(f))) if(length(f[[i]])==n) sv[i]<- TRUE
sv
names(f)[sv]

# Compile information on the components of the fit
info <- data.frame(item=names(f), prop=NA, dim1=NA, dim2=NA, length=NA)
for(i in 1:length(f)){
  n <- names(f)[i]
  p <- as.numeric(object.size(f[[n]])/object.size(f))
  info$prop[i] <- p
  d <- dim(f[[n]])
  info$dim1[i] <- ifelse(is.null(d[1]), NA, d[1])
  info$dim2[i] <- ifelse(is.null(d[2]), NA, d[2])
  info$length[i] <- length(f[[n]])
}
info$percent <- round(info$prop * 100, 2)
info <- info[order(info$prop, decreasing=TRUE), ]
info$prop <- NULL

# Extract the original data from the fit
data <- f$data

# Emprically determine which components of the fit are necessary to make predictions
drop.candidates <- info$item
necessary <- rep(TRUE, length(drop.candidates))
for(i in 1:length(drop.candidates)){
  drop <- drop.candidates[i]
  f2 <- f[!names(f) %in% drop]
  class(f2) <- class(f)
  # Check to make sure it works and produces the same results as the full object
  p <- predict(f, newdata=data)
  a <- tryCatch(p2 <- predict(f2, newdata=data), error=function(e) e)
  if(inherits(a, "error")){
    necessary[i] <- TRUE
    next
  }
  if(isTRUE(all.equal(p, p2))) necessary[i] <- FALSE
}
info$necessary <- necessary

# Drop everything that isn't necessary
drop <- info$item[!info$necessary]
cat("\"", paste(drop, collapse='", "', sep=""), '"', sep="") # Print as string for hardcoding intofunction

# Make final paired down verion of f2
f2 <- f[!names(f) %in% drop]
class(f2) <- class(f)

# Double check predictions
p <- predict(f, newdata=data)
p2 <- predict(f2, newdata=data)
if(!isTRUE(all.equal(p, p2))) stop("The predictions should still match")

# check on size reduction
cat(100- round(as.numeric(object.size(f2)/object.size(f)  ) *100, 2), " % reduction")

# Compile info on the remaining components
info2 <- info[info$necessary, ]
info2$percent <- round(info2$percent/sum(info2$percent) *100, 2)
info2

# See if we can drop the biggest part of qr
f2$qr$qr <- NULL
class(f2$qr) <- class(f$qr)

# Double check predictions
p <- predict(f, newdata=data)
p2 <- predict(f2, newdata=data)
if(!isTRUE(all.equal(p, p2))) stop("The predictions should still match")


# See if I can delete the environent and still make predictions
str(f2)
attr(f2$terms, ".Environment") <- NULL
p2 <- predict(f2, newdata=data)
if(!isTRUE(all.equal(p, p2))) stop("The predictions should still match")
str(f2)

# check on size reduction
cat(100- round(as.numeric(object.size(f2)/object.size(f)  ) *100, 2), " % reduction")

stripglm <- function(x){
  # stripglm ethan plunkett   March 13, 2012
  
  # This function strips a glm down to the bare minimum needed to make a prediction
  # It was built and tested around logistic regression models for the purpose of predicting 
  #  development in LCAD;  I make no claims to broader applicability.
  #  March 20 - added line to wipe the environment of the terms object
  drop <- c("data", "family", "model", "residuals", "fitted.values", "linear.predictors", "weights",
            "prior.weights", "y", "effects", "R", "call", "control", "formula", "xlevels", "method", 
            "deviance", "aic", "null.deviance", "iter", "df.residual", "df.null", "converged", 
            "boundary", "offset", "contrasts")
  x2 <- x[!names(x) %in% drop]  # drop complete list elements
  class(x2) <- class(x) # retain class
  x2$qr$qr <- NULL # drop the qr component of the qr item
  attr(x2$terms, ".Environment") <- NULL  # Drop the .Enviroment component of terms. 
  class(x2$qr) <- class(x$qr) # retain class
  return(x2)
}


stripglmlist <- function(x){
  # This is a function to cut the extra stuff from glm objects as liz has them stored for LCAD  
  for(i in 1:length(x))
    if(length(x[[i]]) > 0) for(j in 1:length(x[[i]]))
      if(inherits(x[[i]][[j]],"glm")) x[[i]][[j]] <- stripglm(x[[i]][[j]])  
  return(x)
}


x <- stripglmlist(LR.fits)

as.numeric(object.size(x)/object.size(LR.fits))
print(object.size(x), units="Mb")
print(object.size(LR.fits), units="Mb")


# Test function
f2 <- cutglm(f)

# Double check predictions
p <- predict(f, newdata=data)
p2 <- predict(f2, newdata=data)
if(!isTRUE(all.equal(p, p2))) stop("The predictions should still match")



