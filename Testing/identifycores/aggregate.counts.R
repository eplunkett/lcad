# Aggregate counts file based on lumped groups

# This is annoying
os <- sum(counts)
grps <- read.table("X:/LCC/LCAD/Testing/identifycores/PostlandGroupedWetlands.par", header=TRUE, sep="\t")
colnames(grps) <- tolower(colnames(grps))
grps <- grps[, grep("value", colnames(grps))]
names(grps) <- c("val", "grp")

ug <- unique(grps$grp)
new.counts <- matrix(NA, nrow=nrow(counts), ncol=length(ug))
colnames(new.counts) <- ug
rownames(new.counts) <- rownames(counts)
i <- 1
for(i in 1:length(ug)){
  g <- ug[i]
  vals <- grps$val[grps$grp %in% g]
  new.counts[, i] <-  apply(counts[,colnames(counts) %in% vals, drop=FALSE], 1, sum)  
}
counts <- counts[ , !colnames(counts) %in% grps$val]

sv <- colnames(new.counts)  %in% colnames(counts)
new.counts.1 <- new.counts[, sv, drop=FALSE] # duplicate colums in counts will be merged
new.counts.2 <- new.counts[, !sv, drop=FALSE] # new columns will be appended

mv <- match(colnames(new.counts.1), colnames(counts))
counts[,mv] <- counts[, mv] + new.counts.1[, ]
counts <- counts[ , !colnames(counts) %in% grps$val] 
counts <- cbind(counts, new.counts.2)

counts <- counts[, apply(counts, 2, function(x) sum(x) > 0)]
stopifnot(sum(counts) == os)
