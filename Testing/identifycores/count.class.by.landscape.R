
# 
#1.  Generate cell counts by cover type (id) and landscape
#

# Setup result structure 
# since I don't know how many systems or landscapes I'm going to encounter I preallocate 
# a bunch of space and then as I encounter new values add their names to previously 
# unused rows and columns.
max.systems <- 500  
first.unused.col <- 1
max.landscapes <- 20
first.unused.row <- 1
counts <- matrix(0, max.landscapes, max.systems)
rownames(counts) <- paste("unused", 1:nrow(counts))
colnames(counts) <- paste("ubysed", 1:ncol(counts))
i <- 56  # for line by linen testing


for(i in 1:ntiles()){
  cat("Tallying tile", i, "of", ntiles(), "\n")
  settile(i)
  mask <- readtile(r.mask.path)
  mask$m[is.na(mask$m)] <- 0
  mask$m <- as.logical(mask$m)  # note: TRUE in core
  if(!any(mask$m)) next
  landscapes <- readtile(r.landscape.path) # note this grid just has 0 and 1 (1 in ctriver, 0 = out) 
  land <- readtile(r.land.path)  
  
  # Extract vectors of values for unmasked cells
  landscape.vals <- landscapes$m[mask$m]
  cover.vals <- land$m[mask$m]
  
  # Update counts matrix to include and new catagories that haven't been encountered in previous tiles
  a <- unique(landscape.vals, incomparables=FALSE)
  a <- as.character(a)
  a[is.na(a)] <- "NA"
  
  # Update rownames of counts
  if(first.unused.row ==1){
    new <- a
  } else {
    new <- setdiff(a, rownames(counts)[1:(first.unused.row-1)])
  }
  n <- length(new)
  if(n!=0){
    rownames(counts)[first.unused.row:(first.unused.row + n -1)] <- new
    first.unused.row <- first.unused.row + n
  }
  # Update colnames counts
  b <- unique(cover.vals, incomparables=FALSE)
  b <- as.character(b)
  b[is.na(b)] <- "NA"
  if(first.unused.col ==1){
    new <- b
  } else {
    new <- setdiff(b, colnames(counts)[1:(first.unused.col-1)])
  }
  n <- length(new)
  if(n!=0){
    colnames(counts)[first.unused.col:(first.unused.col + n -1)] <- new
    first.unused.col <- first.unused.col + n 
  }
  # add counts to matrix
  t <- table(landscape.vals, cover.vals, useNA="ifany")
  t <- t[] # convert to matrix
  sv <- is.na(rownames(t))
  if(any(sv)) rownames(t)[sv] <- "NA"
  sv <- is.na(colnames(t))
  if(any(sv)) colnames(t)[sv] <- "NA"
  
  rmv <- match(rownames(t), rownames(counts)[1:(first.unused.row -1 )] )
  cmv <- match(colnames(t), colnames(counts)[1:(first.unused.col -1 )] )
  counts[rmv, cmv] <- t[, ] + counts[rmv, cmv, drop=FALSE]
  cat("Done with tile ", i, " of ", ntiles(), "\n")
  
}
counts <- counts[1:(first.unused.row-1), 1:(first.unused.col-1)]
save(counts, file=counts.file)
