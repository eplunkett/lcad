# Package Geophysical Settings Raster





original <- "X:/LCC/GIS/Work/LCD/TNCresilience/LCD/setting_geo"

package.name <-  "DSL_geophysical_settings_v1.0" 
package.dir <- paste0("X:/LCC/GIS/Final/PackagesSlim/", package.name, "/")
dir.create(package.dir, showWarnings = FALSE)
dest.tif <- paste0(package.dir, package.name, ".tif")

compress.tif(original, out = dest.tif, build.overviews = TRUE)


zipped.package.dir <- "X:/FTP/web/LCC/"
zip.file <- paste0(zipped.package.dir, package.name, ".zip")

#--------------------------------------------------------------------------------------------------#
# Add VAT
# Note 1. a vat assoiciated with a tif vat is a dbf with "VALUE" and "COUNT" fields containing all the values in the grid and 
# their counts.  Other fields are optional
# 2. in this case I manually exported the vat from within arc to the dbf file below so I'm just 
#  copying the file over
#--------------------------------------------------------------------------------------------------#

vat.original <- "X:/LCC/GIS/Work/LCD/TNCresilience/LCD/setting_geo_VAT_export.dbf"
vat.dest <- paste0(dest.tif, ".vat.dbf")
file.copy(vat.original, vat.dest)


cat("output tif: ", gsub("/", "\\\\", dest.tif), "\n")

zipdir(package.dir, zip.file)


#--------------------------------------------------------------------------------------------------#
# Print list of links
#--------------------------------------------------------------------------------------------------#
pattern <- package.name
l <- list.files(zip.dir, pattern = pattern)
web.base.path <- "http://jamba.provost.ads.umass.edu/web/LCC/"
links <- paste0(web.base.path, l)

lf <- list.files(zip.dir, pattern = pattern, full.names = TRUE)
sizes <- file.size(lf)
d <- data.frame(link = links, size = sapply(sizes, utils:::format.object_size, units=  "auto"))
print(d)
