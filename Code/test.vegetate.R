test.vegetate <- function(scenario, rep, tilesize=800, tile, timestep=2){
  # Setup
  set.scenario(scenario)
  set.rep(rep)
  initialize.gridserver()
  tileinit(tilesize, 0)
  if(missing(tile)) tile <- floor(tilescheme()$cols * (floor(tilescheme()$rows/2)+0.5))  # should be near the middle.
  settile(tile)
  plot.dir <- paste(g("output", 0),"/plots/", sep="")
  
  age <- readtile(g("age", t=timestep))
  
  gg <- p("grow.group")
  
  land <-readtile(g("land", t=timestep))
  group <- swap(land$m, original=gg$value, replacement=gg$group, no.match=NA)
  
  cols <- rainbow(n=length(unique(gg$group)))
  gcols <- cols[match(group, letters)]
  
  
  gt <- p("grow.traj")
  veggrids <- unique(gt$attr)
  match(c("a", "d"), letters)
  for(grid in veggrids){
    # grid <- veggrids[1]
    veg <- readtile(g(grid, t=timestep))
    
  
    if(!file.exists(plot.dir)) dir.create(plot.dir)
    png(width=5, height=5, units="in", res=150, filename=paste(plot.dir, grid,".ts",  timestep, ".png", sep=""))
    
    plot(x=age$m, y=veg$m, xlab="stand age", ylab=grid, cex=0.5, col=gcols)  
    title(main=paste(scenario, rep))
    mtext(text=paste("timestep=", timestep), side=3, line=.5)
    gt2 <- gt[gt$attr == grid, ] 
   
    
    for(i in 1:nrow(gt2)){
      a <-gt2$a[i]
      b <- gt2$b[i]
      
      # Extract the equation for this group and attribute and convert it into a function "fun"
      eq <- gt2$eq[i]
      eq <- gsub("^y[[:blank:]]*=[[:blank:]]*", "", eq)
      eval(parse(text=paste("fun <- function(x, a, b)", eq)))
      
      # Plot the funciton
      curve(fun(x=x, a=a, b=b), add=TRUE, col=cols[i])
    }
    legend("bottomright", legend=gt2$group, col=cols, lty=1, bty="n", ncol=3)
    dev.off()
  }
  cat(paste("Plots in", plot.dir, "\n"))
}
