#-------------------------------------------------------------------------------
# Name:        define.projection
# Purpose:
#  This assignes a projection (without reprojecting) to an existing GIS file
#
# Author:      Ethan  Plunkett
#
#-------------------------------------------------------------------------------
import os
import arcpy
from arcpy import env
# from arcpy.sa import *

input = "[input]"
workspace = "[workspace]"
reference = "[reference]"  # used to definie projection
completeFile = "[complete]"  # created on successfull completion to indicate that it worked.

arcpy.env.workspace = workspace

print "starting projection definition"

# get the coordinate system by describing 
dsc = arcpy.Describe(reference)
coord_sys = dsc.spatialReference
    
# run the tool
arcpy.DefineProjection_management(input, coord_sys)
    
print "done processing"

# Write empty file indicating script completed successfully
open(completeFile, 'a').close()

print "done"
