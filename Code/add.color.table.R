

add.color.table <- function(path, x){
  # This function creates a .vrt file alongside a raster that references the raster and also includes
  # a color table.  .vrt is a GDAL virtual format file that describes a dataset to be composed of 
  # other datasets.  In this case it functions a little like an ESRI .lyr file in that it references
  # the data in the raster while including symbology information.  The .vrt can be read directly by 
  # ESRI GIS software or can be passed to compress.tif (or gdal translate directly) in which case
  # the data and symbology will be united in the output file.  My workflow is generally:
  #   1. add.color.table( )   create .vrt
  #   2. compress.tif()  called directly on the .vrt   
  #   3. (optionally) build a vat.
  #
  #  Arguments
  #    path - the path to a raster file (I've generally used tifs but a grid would probably work too)
  #       it must be a single band byte (AKA INT1U) raster; these files support values 0 to 255. 
  #
  #    x - a data.frame with two or three columns:
  #       value - raster cell values which will be assigned a color
  #       color - the color to assign each value.  It will be passed to col2rgb() so any format  
  #           that function accepts will work.  I generally use a hexidecimal color  #rrggbb format
  #           that is the standard for web design.  Eg:  https://www.w3schools.com/colors/colors_picker.asp
  #       category - (optional) text describing the class.  If present this will be used in the map legend.
  #   
  #  Note: The key will include all values between 0 and the maximum value in the raster.  If 
  #     your raster doesn't use some values in the sequence the missing values will appear
  #     in the key with white as the associated color and an empty string as the category label.
  #     Generally it works best if you recode your raster to have values 0 to N-1 or 1 to N.
  #
  #   Ethan Plunkett
  
  
  library(gdalUtils)  
  stopifnot(c("value", "color") %in% names(x))
  add.categories <- "category" %in% names(x)


  # Check that input file is a single band file with byte (INT1U) data storage
  gi <- GDALinfo(path)
  df <- attributes(gi)$df 
  stopifnot(nrow(df) == 1)
  stopifnot(df$GDType == "Byte")
  
  # Identify the maximum value in attributes
  max.val <-  max(x$value) 
  all.values <- 0:max.val

  
  # vrt file will be the same as the input tiff but with .vrt extension
  vrt.file <- gsub("\\.tif$", ".vrt",path,  ignore.case = TRUE)
  
  if(is.factor(x$color)) x$color <- as.character(x$color)
  
  #--------------------------------------------------------------------------------------------------#
  #  Create color table text for insertion in VRT file
  #--------------------------------------------------------------------------------------------------#
  
  # Make color table vrt text
  # First color corresponds to a value of 0
  # subsequent colors to values 1 to the maximum in the grid
  colors <- rep("#FFFFFFFF", length(all.values))  # start with all colors white
  colors[match(x$value, all.values)] <- x$color     # Fill in supplied colors in right slot
  colors <- as.data.frame(t(col2rgb(colors)))
  color.table <- paste0('  <Entry c1="', colors[, 1], 
                        '" c2="', colors[, 2], 
                        '" c3="', colors[, 3], 
                        '" c4="255"/>', collapse ="\n  " ) # <!-- ', all.values, ' -->
  # Add additional xml 
  color.table<- paste(
    "<ColorInterp>Palette</ColorInterp>", 
    "<ColorTable>", 
    color.table, 
    "</ColorTable>", collapse ="\n", sep="\n")
  
  #--------------------------------------------------------------------------------------------------#
  #  Create catagories text for insertion in VRT file
  #--------------------------------------------------------------------------------------------------#
  if(add.categories){
  classes <-  rep("", length(all.values))
  classes[match(x$value, all.values)] <- as.character(x$category)
  
  categories <- paste(
    "<CategoryNames>",
    paste("  <Category>", classes, "</Category> <!-- ", 0:(length(classes) -1 ), " -->",  collapse = "\n", sep=""),  
    "</CategoryNames>", sep ="\n")
  
  }
  #--------------------------------------------------------------------------------------------------#
  # Create and modify the VRT file
  #--------------------------------------------------------------------------------------------------#
  

  # Make vrt file to use as templace
  gdal_translate(path, vrt.file, of = "VRT")
  
  # Read in template vrt file
  vrt <- readLines(vrt.file)
  vrt <- paste(vrt, collapse = "\n")
  
  
  if(add.categories){
    new.xml <-  paste(color.table, categories, sep ="\n")
  } else {
    new.xml <- color.table    
  }
  
  stopifnot(length(new.xml) == 1)
  
  # Replace <ColorInterp> tag with new xml code
  vrt <- gsub("<ColorInterp>.*</ColorInterp>", new.xml, vrt)
  
  
  # Write vrt file
  writeLines(vrt, vrt.file)
  
  
  # Possibly use VRT file as source for new tif
  # compress.tif(vrt.file, "X:/Working/ethan/temp/test2.tif")
}