copy.layer <- function(layer, grid, suffix=""){
  # This function creates a copy of a layer file that references a new grid.
  # Arguments:
  #   layer : the full path to a layer file
  #   grid : the full path to a grid file
  #   suffix: an optional suffix to be used in nameing the resulting layer; the layer will be named [grid][suffix].lyr
 
  # grid = "W:/DATA/LCC/GISdata/Packages/pocnan/B/pocnan-b1-0-3/postland2030"
  # suffix = ""
  # layer = "X:/LCC/GISdata/DataFinal/caps/pocnan/grids/postland.lyr"
  
  # Note the underlying python funciton doesn't seem to work. It creates a layer file but the "source" for the data end
  # up being completely bogus.  It seems to be an ESRI bug.
  outpath <- paste(grid, suffix, ".lyr", sep="")
  if(file.exists(outpath)) file.remove(outpath)
  
  l <- c(
    '# Script to make a new layer file for a grid based on an existing layer file for another grid.',
    '# Based on ConvertGridToRasterLayerWithSymbology function written by J. Grand',
    paste('# Created by LCAD on', date() ),
    'import arcpy', 
    
    '# Set paths', 
    paste('gridPath = "', grid, '"  # Grid that needs a layer file', sep=""),
    paste('symbologyLayer = "', layer, '"  # The layer file to use for all pilot watersheds; includes all classes.',sep=""),
    paste('outPath = "', outpath, '" # The destination',sep=""),
    
    'arcpy.MakeRasterLayer_management(gridPath,"templayer")',
    'arcpy.SaveToLayerFile_management("templayer",outPath,"RELATIVE")',
    'arcpy.ApplySymbologyFromLayer_management(outPath,symbologyLayer)',
    'print "layer with symbology created for " + gridPath'
  )
  # cat(l, sep="\n")  # Print code to console
  
  
  temp.dir <- "C:/temp/"
  if(!file.exists(gsub("/$", "", temp.dir))) dir.create(path=temp.dir)
  
  temp.file <- paste(temp.dir, round(runif(1, 1, 100000)), ".py", sep="")
  while(file.exists(temp.file))temp.file <- paste(round(runif(1:10000)), ".py", sep="")
  writeLines(l, con=temp.file)
  
  temp.file
  shell(temp.file)
  file.remove(temp.file)  
}

