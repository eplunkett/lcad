
caps.progress <- function(watersheds, climates, reps, timesteps=c(2,7), post=FALSE){
  # Function to report onthe progres ofcaps runs
  # Scan all the result folders
  n <- length(watersheds) * length(climates) * length(reps) * length(timesteps)
  result <- vector(mode="list", length=n)
  timerange <- matrix(data="", ncol=2, nrow=n)
  runs <- rep("", n)
  ts <- rep(NA,n)
  ri <- 0 # index of the result row
  for(watershed in watersheds) for(climate in climates) for(rep in reps) for(timestep in timesteps){
    ri <- ri +1 # increment
    scenario <- paste(watershed, climate, "0",sep="-")
    runs[ri] <- paste(scenario, rep, sep="_")
    ts[ri] <- timestep
    set.scenario(scenario)
    set.rep(rep)
    set.timestep(timestep)
    cb <- gsub("/$", "/", g("capsbase"))

    if(post){
      cr <- paste(cb, "post/scaled/", sep="")
    }else {
      cr <- paste(cb, "results/", sep="")
    }
    if(!file.exists(gsub("/$", "", cr))) next  # skip if the results directory isn't there
    l <- gridlist(cr)
    result[[ri]] <- l
    
    d <- rep(NA, length(l) )
    for(i in 1:length(l)){
      f <-l[i]
      d[i] <- format(file.info(paste(cr, f, sep=""))$mtime)
    }
    timerange[ri, ] <- range(d)
  }
  
  # Make a comprehsive list of results grids 
  grids <- NULL
  for(i in 1:length(l)){
    grids <- union(grids, l[[i]])  
  }
  
  # Convert results into matrix
  result2 <- matrix(FALSE, nrow=n, ncol=length(grids))
  colnames(result2) <- grids
  for( i in 1:n){
      result2[i, match(result[[i]], grids)] <- TRUE
  }
  
  # Combine with times
  colnames(timerange) <- c("first", "last")
  result3 <- cbind(data.frame(run=runs, timestep=ts), as.data.frame(result2), timerange)
  return(result3)
  
  
} # end function definition

