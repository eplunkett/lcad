#set.scenario("ctsub5-a1b-0"); set.timestep(0); set.rep(1); cleanup();gridinit(port=p("port"), source=p("gridserver"))

count.built <- function(tilesize=2000){
  b <- lookup("built")
  result <- rep(0, length(b))
  tileinit(tilesize, 0)
  for(j in 1:ntiles()){
    cat("starting on tile ", j, " of ", ntiles(), "\n")
    settile(j)
    t <- readtile(g("land"))
    for(i in 1:length(b)){
      result[i] <- sum(result[i] + sum(t$m == b[i], na.rm=TRUE), na.rm=TRUE)
    }
  }
  names(result) <- b
  return(result)
}
