#' Function to compress and possibly reformat a tif with options to build overviews and vat
#' 
#' @param file the input file to be compressed
#' @param the compressed output file to be created
#' @param build.overviews if TRUE overviews (pyramids) will be built 
#' @param vat if TRUE a VAT will be built
#' @param type if supplied the output tif will be in this storage type 
#' @param overview.resample the method to use when resampling overviews
#' @param overwrite if TRUE existing file will be overwritten
#' @param predictor the predicor uses in LZW or Deflate compression. 1 is no
#'    predictor, 2 is horizonal differencing, and 3 is floating point prediction.
#'    1 is default and causes no additional arguments to gdal_translate
#' @param a.srs text to be passed as the target spatial reference system 
#'    use "EPSG:5070" for DSL and 
#'        "EPSG:26986" for CAPS
#'
#' @return
#' @export
#'
#' @examples
compress.tif <- function(file, out, build.overviews = TRUE, 
                         vat = FALSE, 
                         stats = TRUE,  
                         type,
                         overview.resample = "nearest", 
                         overwrite = FALSE, 
                         predictor = 1, 
                         a.srs, 
                         gdaltranslate.path = "C:/OSGeo4W64/bin/gdal_translate.exe"){
  # This function creates a compressed copy of a tif with LZW compression, stats, and internal tiling
  #  it will optionally build overview and add a vat
  #  The output should be ready to view quickly and easily in ArcGIS
  #
  
  stop("Please use rasterPrep::makeNiceTif instead of this function. Or tell ethan to turn this error off.")
  
  if(!file.exists(file)) stop("input file", file, "is missing.")
  if(file.exists(out)){
   if(overwrite){
     delete.tif(out)
   } else {
    stop("destination file already exists: ", out)
   }
  }
  
  has.type <- FALSE
  is.signed.byte <- FALSE
  
  if(!missing(type)){
    has.type <- TRUE
    type.no.data <- data.frame(type = c("SIGNEDBYTE",
                                        "Byte", 
                                        "UInt16", 
                                        "Int16", 
                                        "UInt32", 
                                        "Int32", 
                                        "Float32", 
                                        "Float64"),
                               no.data.value = c(
                                 2^7 - 1,  # "SIGNEDBYTE",
                                 2^8 -1,  #"Byte",  
                                 2^16 - 1, # "UInt16", 
                                 -2^15,# "Int16", 
                                 2^32-1, #"UInt32", 
                                 -2^31-1, #"Int32", 
                                 -3.4E+38,# "Float32", 
                                 -1.7E+308), # "Float64"
                               stringsAsFactors = FALSE
    )                          
    if(type == "SIGNEDBYTE"){
      is.signed.byte <- TRUE
      type <- "byte"
    }
    
    stopifnot(tolower(type) %in% tolower(type.no.data$type))
    no.data.value <- type.no.data$no.data.value[tolower(type.no.data$type) == tolower(type)]
    
  } # end missing type
  
  
  qc <- '"'
  command <- paste0(gdaltranslate.path, " -stats -co compress=LZW -co TFW=YES -co TILED=YES ",
                    qc, file, qc, " ", qc, out, qc, " ")
  
  if(has.type)
    command <- paste0(command, 
                      " -ot ", type, " ",
                      " -a_nodata ", no.data.value, " "  # NOTE 
                 )
  if(predictor != 1){
    if(!predictor %in% 2:3) stop("predictor should be 1, 2, or 3.")
    command <-  paste0(command, ' -co PREDICTOR=', predictor, ' ')
  }
    
  
  if(is.signed.byte)
    command <- paste0(command, 
                      " -co  PIXELTYPE=SIGNEDBYTE ")
  
  if(stats)
    command <- paste0(command, "-stats ")
  
  if(!missing(a.srs) && !is.na(a.srs) &&  !a.srs == "") 
    command <- paste0(command, " -a_srs ", qc, a.srs, qc, " ")
  
  # This is a hack to reverse the interference caused by rgdal on 
  # system calls to a different version of GDAL
  #  see: https://github.com/r-spatial/discuss/issues/31
  #
  oproj.lib <- Sys.getenv("PROJ_LIB")
  Sys.setenv("PROJ_LIB" = "")
  on.exit(Sys.setenv("PROJ_LIB" = oproj.lib))
  
  cat("Compressing with system command:\n", command, "\n")
  a <- shell(cmd = command, intern = FALSE, wait = TRUE)  
  
  stopifnot(file.exists(out))
  
  if(build.overviews){
    stopifnot(overview.resample %in% c("nearest", "average","gauss","cubic","cubicspline",
                                       "lanczos","average_mp","average_magphase","mode" ))
    command <- paste0("gdaladdo -ro -r ", overview.resample, " ",
                      qc, out, qc, " 2 4 8 16 32 64 128 256", 
                      " --config COMPRESS_OVERVIEW LZW") 
    cat("Adding overviews with system command:\n", command, "\n")
    a <- system(command = command, intern = TRUE, wait = TRUE)  
  }
  if(vat)
    buildvat(out)
  
  
}