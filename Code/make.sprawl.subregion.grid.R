#--------------------------------------------------------------------------------------------------#
#
# Make sprawl subregion grid
#
#  This function makes a grid where each cell is the ID of the subregion associated with the sprawl
#  pane that the cell fals within.  Having a value in this grid does not gaurentee that a cell is
#  within the region, merely that some cell within that pane is.
#  
#  Ethan Plunkett   May 2016
#--------------------------------------------------------------------------------------------------#

if(FALSE){ 
  rm(list =ls())
  source("X:/LCC/LCAD/load.LCAD.R")
  
  
  
  refgridinit(g("reference"))
  tilesize <- get.tilesize("sprawl")
  subtileinit(rowsize=p("pane.size"), buffer=0, tilesize=tilesize)
  subregion.grid <- "Y:/LCC/GIS/Future/subregions"
  pane.grid <- "Y:/LCC/GIS/Future/devpanes"
  
  
  call <- c("make.sprawl.subregion.grid()", 
            "wait",
            'launchbatchstitch(c(subregion.grid, pane.grid), owner ="ebp", priority = 10, name = "subregion_stitch")')
  
  simple.launch(call = call, owner = "ebp", name ="subregions")
  
  winopen(subregion.grid)

  launchbatchstitch(pane.grid)  
}



make.sprawl.subregion.grid <- function(result.dir = "Y:/LCC/GIS/Future/"){
  con <- saveconnections()
  on.exit(restoreconnections(con))
  
  subregion.grid <- paste0(result.dir, "subregions")
  pane.grid <-  paste0(result.dir, "devpanes")
  
  refgridinit(g("reference"))
  tilesize <- get.tilesize("sprawl")
  subtileinit(rowsize=p("pane.size"), buffer=0, tilesize=tilesize)
  
  srm <- p("subregion.map")
  
  srm.county <- lccpar$subregion.map
  srm.county <- srm.county[srm.county$scenario == "ner.counties", ]
  
  
  for(i in 1:ntiles()){
    settile(i)
    subregions <-  panes <- newtile()
    in.map <- FALSE
    for(j in 1:nsubtiles()){
      setsubtile(j)
      id <- stid()
      if(id %in% srm$pane.id){
        sr.id <- srm$subregion.id[srm$pane.id == id]
        stopifnot(length(sr.id) == 1)
        stopifnot(sr.id > 0)
        stopifnot(sr.id < 100000)
        subregions$m[sti()] <- sr.id
        panes$m[sti()] <- id
        in.map <- TRUE
      }
    } # end j loop
    if(in.map){
      writetile(subregions, subregion.grid, as.integer = TRUE)
      writetile(panes, pane.grid, as.integer = TRUE)
      cat("Done writing tile", i, " of ", ntiles(), "\n")
    }
    
  } # end I loop
  
  if(FALSE){
    # Coarse version (1 cell per pane)
    a <- rasterizetiles(values = srm$subregion.id, subtile.ids = srm$pane.id)
    b <- rasterizetiles(values = srm$pane.id, subtile.ids =  srm$pane.id)
    county <- rasterizetiles(values = srm.county$subregion.id, subtile.ids = srm.county$pane.id)
    
    gridinit()
    setwindow(a)
    writegrid(a, path = paste0(subregion.grid, "C"), as.integer = TRUE)
    writegrid(b, path = "Y:/LCC/GIS/Future/pane_ids", as.integer = TRUE)
    writegrid(county, path = "Y:/LCC/GIS/Future/county_ids", as.integer = TRUE)
    
  }
  
} # end function


