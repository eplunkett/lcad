rename.tif <- function(from, to){
  # Rename a tif file (and it's associated files)
  stopifnot(grepl(".tif$", from, ignore.case = TRUE))
  targets <- find.tif.files(from)
  a <- gsub(".tif$", "", from, ignore.case = TRUE) 
  b <- gsub(".tif$", "", to, ignore.case = TRUE) 
  new <- gsub(a, b, targets)
  print(data.frame(from=targets, to = new))
  file.rename(targets, new)
}