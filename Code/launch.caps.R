if(FALSE){
  source("X:/LCC/LCAD/load.LCAD.R")
  timestep <- 0
  owner = "ebp"
  priority = 0
  set.scenario("baseline")
  set.scenario("plusBoth")
  rep <- 1
  for(rep in 1:3){
    set.rep(rep)
    refgridinit(g("reference"))
    launch.caps(timestep, owner, priority)
  }
}
  
launch.caps <- function(timestep, owner, priority){
  #------------------------------------------------------------------------------------------------#
  #  launch.caps   Ethan Plunkett   April 5, 2012
  #
  #  This function sets up and launches a CAPS run for the current scenario and rep
  #    The tables.par file is tracked separately for each extent as a grid.
  #    capsbase is treated as a grid as well.  
  #  inputs.par is created by helper function write.caps.inputs.
  #    paths.par and parameters.par are both created from versions with a "_lcad.par" suffix by 
  #      helper functions write.caps.paths and update.caps.parameters
  #    metrics_lcad.par, settings_lcad.par, and mixmetrics_lcad.par  are renamed to 
  #       metrics.par, settins.par, and  mixmetrics.par without any other modifications
  #       other contents of the model directory are copied from the LCAD master caps
  #    directory which is specified in the LCAD parameter file: paths.par.
  #  Here's where things are pulled from:
  #    path("caps")  -- "X:/LCC/Parameters/CAPS/"  # for everything but parameters.par, inputs.par, and paths.par
  #    paste(path("caps"), "paths_lcad.par", sep="")  # "X:/LCC/Parameters/CAPS/paths_lcad.par"  by write.caps.paths.R
  #    paste(path("caps"), "model/parameters_lcad.par",sep="") #  "X:/LCC/Parameters/CAPS/model/parameters_lcad.par"  by update.caps.parameters.R
  #    
  #    metrics_lcad.par ->  metrics.par
  #    settings_lcad.par -> settings.par
  #    mixmetrics_lcad.par -> mixmetrics.par
  #
  #   inputs.par created in it's entirety by LCAD
  #
  #   other files in path("caps") are copied as is.
  #------------------------------------------------------------------------------------------------#

  # Setup Directory
  library("anthill")
  if(timestep==0) stop("Initial timestep runs are inputs. We could run them through this function easily but it better be deliberately.")
  set.timestep(timestep)
  d <- g("capsbase")  # destinationpath
  s <- path("caps")  # source path
  d <- gsub("/*$", "/", d)  # enforce trailing "/"
  s <- gsub("/*$", "/", s)

  if(!file.exists(gsub("/$", "", d))) dir.create(d, recursive=TRUE)
  list <- "model/"   # items at the base level of the CAPS directory that should be copied.  If it's a 
     # directory end in "/"
  for(i in list){
    file.copy(paste(s, i, sep=""), d, recursive=TRUE)
  }
  
  # Write brand new inputs.par file  
  inputs <-  write.caps.inputs(timestep=timestep, file=paste(d, "model/inputs.par", sep=""))
  
  # Make a new tilemap (adding lines with updated (current timestep) include grids
  # (subing in future grids in place of current so no rescan is needed)
  # As of July 2022 include is static so this should be just creating a copy
  # of the tilemap.
  new.tilemap.file <- paste0(d, "tilemap.txt")  # same path is set in update.caps.parameters
  make.caps.tilemap(new.tilemap.file)
  
  # Modify paths.par to point to the right grids
  write.caps.paths()
  
  # Update parameters.par (thingslike priority, owner, gridserver etc)
  update.caps.parameters(capsdir=d, owner=owner, priority=priority)
  
  # Check input grids
  window <- getwindow()
  input.problems <- rep(FALSE, nrow(inputs))
  for(i in 1:nrow(inputs)){
    desc <- griddescribe(inputs$path[i])
    if(!coincide(window, desc)) 
      input.problems[i] <- TRUE
  }
  if(any(input.problems)){
    stop(paste("grid", 
               ifelse(sum(input.problems) > 1, "s", ""), 
               " with problems:\n\t",
               paste(inputs$alias[input.problems], ": ",  inputs$path[input.problems],  collapse = "\n\t"),
               "\n\t", sep =""))
  }
  # Rename metrics_lcad and settings_lcad to metrics and settings
  file.rename(paste(d, "model/metrics_lcad.par", sep=""), paste(d, "model/metrics.par", sep=""))
  file.rename(paste(d, "model/settings_lcad.par", sep=""), paste(d, "model/settings.par", sep=""))
  file.rename(paste(d, "model/mixmetrics_lcad.par", sep=""), paste(d, "model/mixmetrics.par", sep=""))
  
  # Make empty run directory (supressing warnings)
  dir.create(paste(d, "run", sep=""), showWarnings = FALSE)
  
  
  dw <- gsub("/", "\\\\", d)  # caps directory path with windows style delimiters
  
  #### And then launch CAPS by adding the run to  caps run and Launching the CAPS workspace.
  # Add run to capsrun list
  a <- gsub("/*$", "/", path("anthill"))  # anthill path with trailing "/"
  l <- paste(a, "capslaunch/", sep="")  # launch directory
  dir.create(l, showWarnings = FALSE)
  f <- paste(l, "capsrun.txt", sep="")
  get.lock(l)
  if(!file.exists(f)){
    con <- file(f, "w")
    writeLines(paste(1, "\t", dw, sep=""), con=con)
    close(con)
    id <- 1
  } else {
    info <- read.table(file=f, header=FALSE, sep="\t", comment.char=";", strip.white=TRUE)
    id <- setdiff(1:(max(info[ ,1])+1), info[ , 1])  # determine new I (use first available unused integer)
    con <- file(f, "a")
    writeLines(paste(id, "\t", dw, sep=""), con=con)
    close(con)
  }
  
  # Call CAPS launching workspace
  ap <- anthill:::.settings$aplpath
  command <- paste( "psexec -i -d ", ap , " ", path("capsworkspace"), " user=", id, sep="")
  command <- gsub("/", "\\\\", command)
  shell(command, wait=FALSE)  
  Sys.sleep(3)
  return.lock(l)
}
