# Note: many of the calls used to build packages with this funciton are in:
# "X:/LCC/LCAD/Packaging/make.single.raster.packages.R"
# Also, as of 2018 most new packages are made with the make.package() function.
# 
#

package.one.raster <- function(input, package.name, build.vat = FALSE, zip = TRUE,
                               packages.dir = "X:/LCC/GIS/Final/PackagesSlim/", 
                               zip.dir = "X:/FTP/web/LCC/newZips/" ){
  # This is designed to make a packages out of a single raster file
  # Arguments:
  #   input : full path to raster file (ESRI grid,  tif, or some other GDAL compatible format)
  #   package.name : will be used for the output tif, the containing directory, and the zip file.
  #   build.vat : if TRUE a vat will be build (Requires ArcPy).  Set true for known categorical grids
  #   zip : if TRUE the package will be zipped
  #   packages.dir : the directory in which the package dir will be created.
  #   zip.dir : the directory in which the zipped package will be created (if zip == TRUE)
  #
  #  Ethan Plunkett  March 2017
  #
  
  # Source raster processing functions
  l <- list.files("Y:/Projects/JFSP/code/functions/", pattern ="\\.[Rr]$", full.names = TRUE)
  a <- lapply(l, source)
  
  dir <- paste0(packages.dir, package.name)
  
  if(file.exists(dir))
    stop("Package directory already exists (", dir, ")")
  
  if(zip){
    zipfile <- paste0(zip.dir, package.name, ".zip")
    if(file.exists(zipfile))
      stop("Zip file already exists: ", zip.file)
  }
  
  dir.create(dir, showWarnings = FALSE)
  raster.out <- paste0(dir, "/", package.name, ".tif")
  
  
  # Copy raster
  compress.tif(input, raster.out, vat = build.vat)
  
  # Zip
  if(zip){
    source.dir <- paste0(packages.dir, package.name, "/") 
    zipdir(zipfile = zipfile, source.dir)
  }
  
}