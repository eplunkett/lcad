
update.habitat.parameters <- function(file, species, owner, priority){
  # Function to update specific parameters in the parameters.par to reflect the current 
  #   scenario rep, project, owner, gridserver, etc.
  # Ethan Plunkett
  #  Sept 13, 2012
  
 
  f <- file
  l <- readLines(f)
  pathR <-g("habitat", species=species, habitat="")
  pathP <- paste(pathR, "run/", sep="")
  
  # Make list of parameters to update (list name is parameter name contents is new value) 
  parameters <- list(  
                    
                     priority=priority, 
                     owner=owner,
                     project= get.habitat.project.name(species),
                     tilemap= gsub("/", "\\\\", path("tilemap")),
                     pathR = gsub("/", "\\\\", pathR), 
                     pathP = gsub("/", "\\\\", pathP),
                     splittasks = "yes"
                     #  port=p("port"), 
                     # server=p("gridserver"), 
                     #refgrid = gsub("/", "\\\\", g("reference", 0)),
                     #maskgrid = gsub("/", "\\\\", g("mask", 0))
                     # timestep=get.timestep(),
                     # year=substr(start=3, stop=4, get.year()),
                     # scenario=paste(p("sres")), 
                     # backup ="no"
  )
  
  # Update values in text from file
  for(i in 1:length(parameters)){
    pn <- names(parameters)[i] # parameter name
    pv <- parameters[[i]]   # parameter value
    if((is.character(pv) | is.factor(pv)) && !pv %in% c("yes", "no"))  
      pv <- paste("'", as.character(pv), "'", sep="")  # add extra quotes around text items (except for yes and no which should be left as is)
    ln <- grep(paste(pn, "[[:blank:]]*=", sep=""), l)
    comment <- gsub("^[^;]*", "", l[ln])
    l[ln] <- paste(pn, " = ", pv, " ", comment, sep="")
  }
  
  # Add comment line indicating LCAD tweaking and date
  l <- c(paste("; Parameter file updated by LCAD on ", date(), sep=""), l)
 
  
  # overwrite file with new values
  file.remove(f)
  writeLines(l, con=f)

}
