if(FALSE){
  source("X:/LCC/LCAD/Testing/load.LCAD.R")
  set.scenario("ner-test")
  set.timestep(0)
  library(anthill);config()
  
  input <- "X:/LCC/GIS/Work/Landcover/ESMplus/ner/luwet_m"
  mask <- "X:/LCC/GIS/Work/Ancillary/Mask/ner/lcad_m"
  
}


makemask <- function(input="X:/LCC/GIS/Work/Landcover/ESMplus/ner/luwet_m", 
                   mask="X:/LCC/GIS/Work/Ancillary/Mask/ner/lcad_m"){
  # This function makes a binary mask in which cells outside the LCC and ocean cells are FALSE
  # and all other cells are TRUE
  
  mask.out <- lookup("ocean")
  tilesize <- 2500  
  refgridinit(input, both=FALSE)
  if(griddescribe(mask)$type!="missing") stop("The mask grid '", mask, "' already exists.")
  tileinit(tilesize, 0)
  i <- 109
  for(i in 1:ntiles()){
    cat(i, "\n")
    settile(i)  
    t <- readtile(input, cacheok=TRUE)
  
    m <- swap(t, original=mask.out, replacement=rep(FALSE, length(mask.out)), na.value=FALSE, no.match=TRUE)
    writetile(m, path=mask, as.mosaic=TRUE, as.integer=TRUE, transparent=FALSE)
  } #end loop through tiles
}