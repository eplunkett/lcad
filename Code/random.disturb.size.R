# Define disturbance function
random.disturb.size <- function(n){
	shape <- p("size.shape")
	scale <- p("size.scale")
	factor <-p("size.factor")
	trainingcellsize <- p("size.trainingcellsize")
	outcellsize <- p("cellsize", table="model.control")
	result <- NULL
	while(length(result) < n){
		n2 <- n-length(result)
		random <- rweibull(round(n2*factor*1.1), shape=shape, scale=scale)
		random <- random[random > .5]
		result <- c(result, random)
	}
	result <- result[1:n]
	result <- result * (trainingcellsize^2) / (outcellsize^2)
	result <- round(result)
	return(result)
}
