lcc.parameters <- function(){
	# Function to print the current parameters
	for(i in ls(envir=lccpar)){
		print(paste(i, "= "))
		print(get(i, envir=lccpar))	
		cat("\n\n")
	}
}  
