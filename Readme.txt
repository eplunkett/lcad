

This repository contains R code used in the Designing Sustainable Landscapes
project.

LCAD stands for Landscape Change Assessment and Design and was an earlier name 
for the project, that captures a lot of what the model does.

The R code and parameters here are best viewed as one component of the overall 
LCD modeling framework which is a complex system of interrelating models and 
data. 

There are terrabytes of GIS files that are referenced by the functions and 
parameters here but which are not included in this repository.

Not included in this repository
	1. Two R packages that house generic functions that were developed for 
	this project: 
		A. anthill - https://bitbucket.org/eplunkett/anthill is an R 
		package task management and parallel computing 
		suited to jobs that take a long time to run and  can be broken 
		into jobs taking many minutes to several hours each.
		
		B gridio - https://bitbucket.org/eplunkett/gridio is an R package 
		for reading and writing and processing raster data in parallel. It 
		contains raster processing functions written for the LCD project.
		
	2. Major LCD sub models:

		A. CAPS  (The conservation assessment and prioritization) model is 
		written in APL. The R code in here (in this LCAD) repository manages 
		launching CAPS but doesn't run CAPS itself.

		B. HABITAT - Is also a separate APL program for modeling species 
		habitat capability which is
		similarly called from LCAD.

		C. Ecosystem cores are also created with APL code (integrated with 
		CAPS) 

		D. Assessing regional connectivity among cores, building corridors, 
		and assessing various impacts of growth is similarly integrated with 
		CAPS.
		
	3. Data - These models require terabytes of spatial data to run on the 
       Northeast Region (NER).
	   
	    

What is here. 
	Code to model urban growth. 
		1. The SPRAWL model of urban growth is integrated into LCAD. Largely 
		it is in functions defined within the  /Code subdirectory while 
		launching scripts are  in /Running
		
		2. Code to build species cores most of this in the /Design 
		subdirectory.
		
		3. Code to manage suites of runs with varying parameters, to track 
		raster files, and to launch CAPS and HABITAT on different scenarios, 
		reps, and time steps.
		
		4. Code to calculate statistics and make raster files that summarize 
		runs /Stats contains files for launching individual calculations.
		
		5. Parameter files for all of the above within /Parameters
	
	
I'm sharing this code for the sake of transparency.  I don't expect that it 
will de easy to reproduce or run outside of the Umass Landscape Ecology Lab 
cluster where the GIS data and ancillary models it depends on are housed.   
	
Dec 3, 2018
Ethan Plunkett
ethan@plunkettworks.com  eplunkett@eco.umass.edu
		

Index to subdirectories
	Code : files defining functions that make up the model (Sourcing code 
		doesn't actually execute any analysis it just defines functions)
	Parameters : Files that define the parameterization of the LCAD model.  
	Testing : Older code that was used to test or run the LCAD model.  
		Sourcing files in this directory generally does stuff.
	Running : Code used  run the LCAD model. Sourcing code here launches
	    projects on Anthill. 
	Packaging : Code to package output grids for distribution as tifs
	Design : Code to implement aspects of the DSL CTR pilot.  In particular 
	the species core generation code and code to build statistics on cores.
	