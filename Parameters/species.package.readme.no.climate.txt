Designing Sustainable Landscapes (DSL) products for [name] ([species])

Note this species does not have a climate niche model so none of the products relating to climate are available for it.

Products that represent landscape capability:
� [species]_LC_2010_v[ver].tif - Landscape capability 2010. This represents the ability of the landscape to support the species given 2010 landcover.	
� [species]_LC_2080_v[ver].tif - Landscape capability 2080. This represents the ability of the landscape to support the species given 2080 landcover.

The units for LC_2010, LC_2080 are both landscape capability and thus directly comparable. 
The rasters that we use internally (and some previously distributed version) are floating point and range from 0 to 1. To greatly reduce file size we've converted them to integers between 0 and 1000.
In the process we forced all values greater than zero and less than .001 in the original to 1; thus preserving non-zero status.  All other values were multiplied by 1000 and rounded.

Landscape capability is an index of the ability of the landscape to support populations of the species.

You may download this data and an abstract for [name] from the DSL project scholar works site:
	https://scholarworks.umass.edu/designing_sustainable_landscapes

The technical document on species modeling is here: 
	https://scholarworks.umass.edu/designing_sustainable_landscapes_techdocs/9/
