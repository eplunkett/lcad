# Script to make a new layer file for a grid based on an existing layer file for another grid.
# Based on ConvertGridToRasterLayerWithSymbology function written by J. Grand 

import arcpy,os

# Set paths I'll overwrite these with R
gridPath = "W:/DATA/LCC/GISdata/Packages/pocnan/B/postland2010" # Grid that needs a layer file
symbologyLayer = "X:/LCC/GISdata/Output/postland_layer.lyr"  # The layer file to use for all pilot watersheds; includes all classes.
outPath = "W:/DATA/LCC/GISdata/Packages/pocnan/B/postland2010.lyr" # The destination


arcpy.MakeRasterLayer_management(gridPath,"layer")
arcpy.SaveToLayerFile_management("layer",outPath,"RELATIVE")
arcpy.ApplySymbologyFromLayer_management(outPath,symbologyLayer) 
print "layer with symbology created for " + gridPath
