# Script to make a new layer file for a grid based on an existing layer file for another grid.
# Based on ConvertGridToRasterLayerWithSymbology function written by J. Grand 

import arcpy,os

# Set paths I'll overwrite these with R
gridPath = "W:/LCC/GISdata/Packages/pocnan/B/postland2010" # Grid that needs a layer file

# layer file to re-path
fname = "W:/DATA/LCC/GISdata/Packages/pocnan/B/pocnan-b1-0-3/postland2030.lyr" # The destination

# new path to workspace containing the feature class
target_wspace = "W:/LCC/GISdata/Packages/pocnan/B/"


lyr = arcpy.mapping.Layer(fname)

fixed_fname = os.path.join(savedir, lyr.longName)

print '\nOld layer properties (%s)' % (fname)
print 'workspace:\t', lyr.workspacePath
print 'full path:\t', lyr.dataSource

try:
    lyr.replaceDataSource(target_wspace, 'FILEGDB_WORKSPACE', lyr.datasetName, True)
    lyr.saveACopy(fixed_fname)
except:
    print arcpy.GetMessages()

print '\nNew layer properties (%s)' % (fixed_fname)
print 'workspace:\t', lyr.workspacePath
print 'full path:\t', lyr.dataSource


del lyr




layer

arcpy.MakeRasterLayer_management(gridPath,"layer")
arcpy.SaveToLayerFile_management("layer",outPath,"RELATIVE")
arcpy.ApplySymbologyFromLayer_management(outPath,symbologyLayer) 
print "layer with symbology created for " + gridPath
