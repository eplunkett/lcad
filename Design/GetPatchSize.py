#GetPatchSize.py
#J. Grand, Oct. 22, 2014
# This script identifies patches of contiguous cells, assigns them a unique
# value and records a cell count for each patch in the outGrid VAT
# Edited by Ethan Plunkett Oct 23:
#       creates binary intermediate grid
#       exports patches VAT as csv

# Parameters:
#  inGrid: input grid
#  outGrid: output grid
#  neighbor rule: "FOUR" or "EIGHT"
#  zoneConnectivity: "CROSS" = cells of any value (except excluded) will be
#     grouped; or "WITHIN" = The only cells that will be grouped are cells of
#     the same value
#  excluded: background value; set non-patch cells to zero (or any number not
#     included in cores); required if zoneConnectivity = "CROSS"

import arcpy,sys,os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")


baseDir  = "X:/LCC/GIS/Work/LCD/species/Oct22_40k80x5ps/"

inGrid = baseDir + "fullgrids/cores_s"  # or set workspace and use grid name only
rcGrid = baseDir + "fullgrids/bincores_s"
outGrid = baseDir + "fullgrids/patches_s" # or set workspace and use grid name only
outCSV = baseDir + "patches.csv"

if arcpy.Exists(rcGrid):
    arcpy.Delete_management(rcGrid)
if arcpy.Exists(outGrid):
    arcpy.Delete_management(outGrid)

neighborRule = "EIGHT"
zoneConnectivity = "WITHIN"

rc = Reclassify(inGrid, "Value", RemapRange([[1, 100000, 1]]), "DATA")
rc.save(rcGrid)
print "finished reclassifying cores"

outRG = RegionGroup(rcGrid, neighborRule, zoneConnectivity)
outRG.save(outGrid)
print "finished creating patches"


print "exporting VAT to CSV"
lstFlds = arcpy.ListFields(outGrid)
header = ''
for fld in lstFlds:
    header += ", " + format(fld.name)

header = header[1:] # drop initial ","

if len(lstFlds) != 0:
    f = open(outCSV,'w')
    f.write(header + '\n')

    rows = arcpy.SearchCursor(outGrid)
    for row in rows:
        rowText = ""
        for field in lstFlds:
            rowText = rowText + str(row.getValue(field.name)) + ", "
        rowText = rowText[:-2]
        f.write(rowText +  '\n')
    f.close()
print "finished exporting VAT to CSV"
