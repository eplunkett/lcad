#--------------------------------------------------------------------------------------------------#
# Script to calculate statistics comparing regional to huc based cores
# Ethan Plunkett
# 7/20/2016
#--------------------------------------------------------------------------------------------------#

#--------------------------------------------------------------------------------------------------#
# Setup
#--------------------------------------------------------------------------------------------------#

library(dplyr)
source("X:/LCC/LCAD/load.LCAD.R")



# Tile map
hi <- p("huc6.info")
tiles <- lapply(hi$tiles, expand.range)
tiles <- unique(unlist(tiles))

# Paths
huc6_cores <- "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/tu-20g_m"
regional_cores <-  "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/tureg-20g_m"
postland <- "X:/LCC/GIS/Final/ner/caps_phase3/grids/postland_m"
huc6 <- g('huc6')
huc.si <-   "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/sel_tur_h6"  # huc scaled selection index
reg.si <-   "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/sel_tur_f"  # regionaly scaled selection index
key <- "X:/LCC/parameters/KeyPhase3.csv"
setwd("X:/LCC/GIS/Final/RCOA/Spring2016/stats/huc-reg-comparison/")

#--------------------------------------------------------------------------------------------------#
# Compare Cores by HUC:
#--------------------------------------------------------------------------------------------------#

# Regional Level
result <- "X:/LCC/GIS/Final/RCOA/Spring2016/stats/huc-reg-comparison/overlap.Rdata"
if(!file.exists(result))
pgridtable(c(huc6_cores, regional_cores), result = result, owner = "Ebp", tilesize = 2000, tiles = tiles  )


load(result)
mean.core.cells <- mean(c(sum(d[ ,1 ]), sum(d[ 1, ])))
pct.overlap <- d[1,1] / mean.core.cells * 100

# By Huc
result <- "X:/LCC/GIS/Final/RCOA/Spring2016/stats/huc-reg-comparison/overlap2.Rdata"
if(!file.exists(result)) # it's already been launched and run
  pgridtable(c(huc6_cores, regional_cores, huc6), result = result, owner = "Ebp", tilesize = 2000, tiles = tiles  )
load(result)
dn <- dimnames(d)
names(dn) <- c("huc_cores", "regional_cores", "huc")
dimnames(d) <- dn

r <- data.frame(huc = as.numeric(unique(dimnames(d)$huc)), 
                cells.in.huc = NA_integer_, 
                regional.core.pct = NA_real_ , 
                huc.core.pct = NA_real_, 
                overlap.pct = NA_real_,
                pct.core.overlap = NA_real_,
                pct.agreement = NA_real_)

for( i in 1:nrow(r)){
  h <- d[ , , i]
  r$cells.in.huc[i] <- sum(h)
  r$huc.core.pct[i] <-   round(sum(h[1, ]) / sum(h) * 100 , 1)
  r$regional.core.pct[i] <- round(sum(h[, 1]) / sum(h) * 100, 1)
  r$overlap.pct[i] <- round(h[1, 1] / sum(h) * 100, 1)
  r$pct.core.overlap[i] <- round(h[1,1] * 2 / (sum(h[1, ]) + sum(h[ , 1])) * 100, 1)
  r$pct.agreement[i] <- round(sum(diag(h) / sum(h)) * 100, 1)
}

r <- r[!is.na(r$huc), ]


file <- "overlap.by.huc.csv"
write.csv(r, file, row.names = FALSE)

md <- c(
  paste0("file = ", file),
  paste0("Created on ", date()), 
  "Columns descriptions:",
  "\thuc: the HUC6 ID of the focal huc",
  "\tcells.in.huc: the number of cells in the huc",
  "\tregional.core.pct: the percent of the huc that is in the regional cores",
  "\thuc.core.pct: the percent of the huc that is in the huc based cores",
  "\tpct.overlap: the percent of the huc that falls within both the huc and regional cores",
  "\tpct.core.overlap: the percentage of the core area that overlaps",
  "\tpct.agreement: the percentage of the huc where the two sets of cores agree (both indicate core or both indicate no core)"
)
writeLines(md, paste0(file, ".medatdata.txt"))

#--------------------------------------------------------------------------------------------------#
# Species Stats
#--------------------------------------------------------------------------------------------------#

huc6_cores <- "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/tu-20g_m"
regional_cores <-  "X:/LCC/GIS/Final/RCOA/Spring2016/Ecosystem/tureg-20g_m"

si <- p("species.info")
spp <- si$species[si$complete & si$core.building]
spp.paths <- get.lc.paths(spp, "initial")

run.table <- expand.grid(spp.index = 1:length(spp), scaling = c("huc", "regional"), stringsAsFactors = FALSE)
run.table <- cbind(data.frame(run = 1:nrow(run.table)), run.table)

out.dir <- "X:/LCC/GIS/Final/RCOA/Spring2016/stats/huc-reg-comparison/lc/"
dir.create(out.dir, showWarnings = FALSE)

par <- list(spp = spp, spp.paths = spp.paths, huc6 = huc6, huc6_cores = huc6_cores, regional_cores = regional_cores, run.table = run.table, out.dir = out.dir, tiles = tiles)

fun <- function(i, par){
  library(gridio2)
  refgridinit(par$huc6)
  spp.index <- par$run.table$spp.index[i]
  scaling <- par$run.table$scaling[i]
  if(scaling == "regional")
    d <- gridstats(x = par$spp.paths[spp.index], by = list(huc = par$huc6, regional_cores = par$regional_cores), tilesize = 2000,  tiles = par$tiles)
  else if(scaling == "huc")
    d <- gridstats(x = par$spp.paths[i], by = list(huc = par$huc6, huc6_cores = par$huc6_cores), tilesize = 2000, tiles = par$tiles)
  else 
    stop("scaling not recognized")
  write.csv(d, row.names = FALSE, file = paste0(par$out.dir, par$spp[spp.index], ".", scaling, ".csv"))
}

if(FALSE){
  # Local test using 2 tiles
  par2 <- par
  par2$tiles <- tiles[1:2]
  fun(1, par2)
}

if(FALSE){ # Already run
  simple.launch(call = "fun(par)", subtaskarg =  "i", subtask = make.range(nrow(par$run.table)),
                owner = "EBP", name = "spp_stats_by_huc", objects = c("fun", "par"), maxthreads = 5)
  
}

# Aggregate Results
lc.reg  <- lc.by.huc <- vector(mode = "list",length = length(par$spp)) 
for(i in seq_along(par$spp)){
  a <- read.csv(paste0(par$out.dir, par$spp[i], ".regional.csv"))
  a <- group_by(a, huc)
  a <- summarize(a, huc.lc = sum(sum), core.lc = first(sum), core.pct.lc = core.lc / huc.lc * 100)
  a$species <- spp[i]
  a$scale <- "regional"
  a <- rename(a, reg.core.lc = core.lc, reg.core.pct.lc = core.pct.lc)
  
  b <- read.csv(paste0(par$out.dir, par$spp[i], ".huc.csv"))
  b <- group_by(b, huc)
  b <- summarize(b, huc.lc = sum(sum), core.lc = first(sum), core.pct.lc = core.lc / huc.lc * 100)
  b$species <- spp[i]
  b$scale <- "huc"
  b <- rename(b, huc.core.lc = core.lc, huc.core.pct.lc = core.pct.lc)
  
  c <- cbind(select(a, -scale), select(b, -huc, -species, -scale, -huc.lc) )
  c <- c[!is.na(c$huc), c("huc",  "species", "huc.lc", "huc.core.lc", "huc.core.pct.lc", "reg.core.lc", "reg.core.pct.lc")]
  lc.by.huc[[i]] <- c
  
  d <- c( list(species = spp[i]),  as.list(apply(c[ , c("huc.lc", "huc.core.lc", "reg.core.lc") ], 2, sum)))
  d <- as.data.frame(d)
  d <- rename(d, tot.lc = huc.lc)
  lc.reg[[i]] <- d
}

lc.reg <- do.call(rbind, lc.reg)
lc.reg <- mutate(lc.reg, huc.core.pct.lc = round(huc.core.lc / tot.lc * 100, 1),
                 reg.core.pct.lc = round(reg.core.lc / tot.lc *100, 1),
                 diff = reg.core.pct.lc - huc.core.pct.lc)

lc.reg <- lc.reg[order(as.character(lc.reg$species)), ,drop = FALSE]
file <- "regional.lc.summary.csv"
write.csv(lc.reg, file = "regional.lc.summary.csv", row.names = FALSE)

md <- c(
  paste0("file = ", file),
  paste0("Created on ", date()), 
  "Columns descriptions:",
  "\tspecies: The focal species code",
  "\ttot.lc: The sum of lc for the species within the region",
  "\thuc.core.lc: The sum of lc within the huc based cores across the region",
  "\treg.core.lc: The sum of lc within the regional based cores across the region",
  "\thuc.core.pct.lc: The percent of the regional lc that falls within the huc based cores",
  "\treg.core.pct.lc: The percent of the regional lc that falls within the region based cores",
  "\tdiff: The difference between reg.core.pct.lc and huc.core.pct.lc")

writeLines(md, paste0(file, ".medatdata.txt"))
    
  
lc.by.huc <- do.call(rbind, lc.by.huc)
lc.by.huc <- arrange(lc.by.huc, species, huc)
lc.by.huc <- mutate(lc.by.huc, pct.diff = reg.core.pct.lc - huc.core.pct.lc, lc.diff = reg.core.lc - huc.core.lc )
lc.by.huc <- lc.by.huc[, c("huc", "species", "huc.lc", "huc.core.lc", "reg.core.lc", 
                           "huc.core.pct.lc", "reg.core.pct.lc",  "lc.diff", "pct.diff")]
file <-  "huc.lc.summary.csv"
write.csv(lc.by.huc, file = file, row.names = FALSE)
md <- c(
  paste0("file = ", file),
  paste0("Created on ", date()), 
  "Columns descriptions:",
  "\thuc: the HUC6 ID of the focal huc",
  "\tspecies: The focal species code",
  "\thuc.lc: The sum of lc for the species within the entire focal huc",
  "\thuc.core.lc:  The sum of lc within the huc based cores (and within the huc)",
  "\treg.core.lc: The sum of lc within the regional based cores (and within the huc)",
  "\thuc.core.pct.lc: The percent of the huc's lc that falls within the huc based cores",
  "\treg.core.pct.lc: The percent of the huc's lc that falls within the region based cores",
  "\tlc.diff: The difference between reg.core.lc and huc.core.lc",
"\tpct.diff: The difference between reg.core.pct.lc and huc.core.pct.lc")
writeLines(md, paste0(file, ".medatdata.txt"))


#--------------------------------------------------------------------------------------------------#
# Launch projects to generate raw data on SI (regionally scaled and huc scaled) and two sets of cores
#--------------------------------------------------------------------------------------------------#
dir <- getwd()
files <- list(
  # calculated for region (won't group by huc but running on huc to mask out stuff outside of hucs)
  reg.si.reg.cores = file.path(dir, "reg.si.by.reg.cores.Rda"),
  reg.si.huc.cores = file.path(dir, "reg.si.by.huc.cores.Rda"),

  # calculated for hucs 
  huc.si.reg.cores = file.path(dir, "huc.si.by.reg.cores.Rda"),
  huc.si.huc.cores = file.path(dir, "huc.si.by.huc.cores.Rda")
)
  
# Read in key file
k <- read.csv(key)
k <- k[ , c("ecosystem_value", "formation", "ecosystem")]
k <- k[!duplicated(k), ]

if(!file.exists(files$reg.si.reg.cores))
  pgridstats(reg.si, by = list(regional_cores = regional_cores, system = postland, huc= huc6),
             tilesize = 2000, tiles = tiles,
             result.file = files$reg.si.reg.cores, 
             owner = "ebp", name = "reg_si_reg_core")

if(!file.exists(files$reg.si.huc.cores))
  pgridstats(reg.si, by = list(huc_cores = huc6_cores,  system = postland, huc= huc6), 
             tilesize = 2000, tiles = tiles,
             result.file = files$reg.si.huc.cores, 
             owner = "ebp", name = "reg_si_huc_cores")

if(!file.exists(files$huc.si.reg.cores))
  pgridstats(huc.si, by = list(regional_cores = regional_cores, system = postland, huc = huc6), 
             tilesize = 2000, tiles = tiles,
             result.file = files$huc.si.reg.cores, 
             owner = "ebp", name = "huc_si_reg_cores")

if(!file.exists(files$huc.si.huc.cores))
  pgridstats(huc.si, by = list(huc_cores = huc6_cores, system = postland, huc = huc6), 
             tilesize = 2000, tiles = tiles,
             result.file = files$huc.si.huc.cores, 
             owner = "ebp", name = "huc_si_huc_cores")

#--------------------------------------------------------------------------------------------------#
# Process the regionally scalled selection index to create a regional summary by system.
# To answer the question of the relative performance of the two sets of cores in capturing 
# regional value.
#--------------------------------------------------------------------------------------------------#
rr <- readRDS(files$reg.si.reg.cores)
rr <- rr[!is.na(rr$huc), , drop = FALSE]
rrg <- group_by(rr, system)
rrs <- summarise(rrg, tot.si = sum(sum), mean.si = sum( count * mean) / sum(count), region.cells = sum(count))
rrf <- filter(rr, !is.na(regional_cores) )
rrfg <- group_by(rrf, system)
rrfs <- summarise(rrfg, reg.core.tot.si = sum(sum), reg.core.mean.si = sum( count * mean) / sum(count), reg.core.cells = sum(count))
rr <- merge(rrs, rrfs, by = "system", all.x = TRUE, all.y = TRUE)

rh <- readRDS(files$reg.si.huc.cores)
rh <- rh[!is.na(rh$huc), ]
rhf <- filter(rh, !is.na(huc_cores))
rhfg <- group_by(rhf, system)
rhfs <- summarise(rhfg, huc.core.tot.si = sum(sum), huc.core.mean.si = sum( count * mean) / sum(count), huc.core.cells = sum(count))
rh <- rhfs

r <- merge(rr, rh, by = "system", all.x = TRUE, all.y = TRUE)

no.na.cols <- c("reg.core.tot.si", "reg.core.mean.si", "reg.core.cells", "huc.core.tot.si", "huc.core.mean.si", "huc.core.cells")
for(col in no.na.cols)
  r[ is.na(r[, col]), col]  <- 0


r <- mutate(r, 
       huc.core.pct.area = round(huc.core.cells / region.cells * 100, 1),
       reg.core.pct.area = round(reg.core.cells / region.cells * 100, 1),
       area.diff = reg.core.pct.area - huc.core.pct.area, 
       reg.core.pct.si   = round(reg.core.tot.si / tot.si * 100, 1),
       huc.core.pct.si   = round(huc.core.tot.si / tot.si * 100, 1),
       si.diff = reg.core.pct.si - huc.core.pct.si
)

r <- rename(r, code = system)

stopifnot(all(r$code %in% k$ecosystem_value))

r <- merge(r, k, by.x = "code", by.y = "ecosystem_value")
first.cols <- c("code", "formation", "ecosystem")
r <- r[, c(first.cols, names(r)[!names(r) %in% first.cols])]
r <- rename(r, ecological.system = ecosystem)

final.cols <- c("code", "formation", "ecological.system", "tot.si", "mean.si",  "region.cells",
                "huc.core.tot.si", "huc.core.mean.si", "huc.core.cells", 
                "huc.core.pct.area",  "huc.core.pct.si", 
                "reg.core.tot.si", "reg.core.mean.si", "reg.core.cells", 
                "reg.core.pct.area", "reg.core.pct.si",
                "area.diff","si.diff")

stopifnot(all(final.cols %in% names(r)))
r <- r[, final.cols] # reorder

file <-  "regional.si.summary.csv"
write.csv(r, file = file, row.names = FALSE)
md <- c(
  paste0("file = ", file),
  paste0("Created on ", date()), 
  "All calculations in this file are based on the regionally scaled selection index (si)",
  "and include data from the entire region.  They are intended to answer the question, ",
  "How much of the regional value is lost when we build cores by huc (to increase distribution)",
  "relative to when we build cores regionally?",
  "",
  "Columns descriptions:",
  "\tcode: The value used to represent the ecological system in grids",
  "\tformation: The formation to which the ecological system belongs",
  "\tecological.system: The focal ecological system for which stats are provided in the row",
  "\ttot.si: The sum of the selection index for the focal system",
  "\tmean.si:  The mean value of the selection index in the focal system",
  "\tregion.cells: The number of cells in the system within the region",
  
  "\thuc.core.tot.si: The sum of the selection index within the huc based cores and the focal system.",
  "\thuc.core.mean.si: The mean of the selection index within the huc based cores and the focal system",
  "\thuc.core.cells: The count of cells within the huc based cores and the focal system",
  "\thuc.core.pct.area: The percent of the area of the system that falls within the huc based cores.", 
  "\thuc.core.pct.si: The perecentage of the total selection index of the system that is captured by the huc based cores", 
 
    "\treg.core.tot.si: The sum of the selection index within the regional cores and the focal system.",
  "\treg.core.mean.si: The mean of the selection index within the regional cores and the focal system",
  "\treg.core.cells: The count of cells within the regional cores and the focal system",
  "\treg.core.pct.area: The percent of the area of the system that falls within the regional cores.",
  "\treg.core.pct.si: The perecentage of the total selection index of the system that is captured by the regional cores", 
  "\tarea.diff: The difference between reg.core.pct.area and huc.core.pct.area",
  "\tsi.diff: The difference between reg.core.pct.si and huc.core.pct.si"
  )
writeLines(md, paste0(file, ".medatdata.txt"))

#--------------------------------------------------------------------------------------------------#
#  Huc level summary of area and HUC scale SI for evaluating how much of the locally good stuff is 
# captured by each set of cores.
#--------------------------------------------------------------------------------------------------#
hh <- readRDS(files$huc.si.huc.cores)
hh <- filter(hh, !is.na(huc)) # drop data corresponding to cells outside of region
hh <- mutate(hh, id = paste0(huc, "-", system)) # add huc-system id colum

hhg <- group_by(hh, id)                             
hhgs <- summarise(hhg, huc = first(huc), system = first(system),  huc.cells = sum(count), huc.si = sum(sum), huc.mean.si = huc.si / huc.cells)

hhf <- filter(hh, !is.na(huc_cores))  # filter to just data on the cores
hc <- rename(hhf, huc.core.cells = count, huc.core.si = sum, huc.core.mean.si = mean)
hc <- hc[ , c("id", "huc.core.cells", "huc.core.si", "huc.core.mean.si")]

h <- merge(hhgs, hc, by = "id", all.x = TRUE, all.y = TRUE)

hr <- readRDS(files$huc.si.reg.cores)
hr <- filter(hr, !is.na(huc)) # drop data corresponding to cells outside of region
hr <- mutate(hr, id = paste0(huc, "-", system)) # add huc-system id colum
hrf <- filter(hr, !is.na(regional_cores))
rc <- rename(hrf, reg.core.cells = count, reg.core.si = sum, reg.core.mean.si = mean)
rc <- rc[, c("id", "reg.core.cells", "reg.core.si", "reg.core.mean.si")]

res <- merge(h, rc, by = "id")
res <- mutate(res, 
            huc.core.pct.area = round(huc.core.cells / huc.cells * 100, 1),
            reg.core.pct.area = round(reg.core.cells / huc.cells * 100, 1),
            reg.core.pct.si   = round(reg.core.si / huc.si * 100, 1),
            huc.core.pct.si   = round(huc.core.si / huc.si * 100, 1),
            area.diff = reg.core.pct.area - huc.core.pct.area, 
            si.diff = reg.core.pct.si - huc.core.pct.si
)
res <- rename(res, code = system)
stopifnot(all(res$code %in% k$ecosystem_value))
res <- merge(res, k, by.x = "code", by.y = "ecosystem_value")
res <- rename(res, ecological.system = ecosystem)

final.cols <- c("huc", "code", "formation", "ecological.system", 
                "huc.si", "huc.mean.si", "huc.cells", 
                 "huc.core.si", "huc.core.mean.si", "huc.core.cells", "huc.core.pct.area", "huc.core.pct.si",
                 "reg.core.si", "reg.core.mean.si", "reg.core.cells", "reg.core.pct.area", "reg.core.pct.si", 
                "area.diff", "si.diff")
stopifnot(all(final.cols %in% names(res)))
res <- res[, final.cols]

file <-  "huc.si.summary.csv"
write.csv(res, file = file, row.names = FALSE)
md <- c(
  paste0("file = ", file),
  paste0("Created on ", date()), 
  "All calculations in this file are based on the HUC6 scaled selection index (si)",
  "each row summarized a single ecoystem for a single huc.  They are intended to answer the question, ",
  "How much of the locally important ecological value is captured by the regional vs the huc based cores?",
  "",
  "Columns descriptions:",
  "\thuc: The HUC6 id of the focal huc",
  "\tcode: The value used to represent the ecological system in grids",
  "\tformation: The formation to which the ecological system belongs",
  "\tecological.system: The focal ecological system for which stats are provided in the row",
  "\thuc.si: The sum of the selection index for the focal system",
  "\thuc.mean.si:  The mean value of the selection index in the focal system",
  "\thuc.cells: The number of cells in the system within the region",
  
  "\thuc.core.si: The sum of the selection index within the huc based cores, the focal system, and the focal huc",
  "\thuc.core.mean.si: The mean of the selection index within the huc based cores,the focal system, and the focal huc",
  "\thuc.core.cells: The count of cells within the huc based cores, the focal system, and the focal huc",
  "\thuc.core.pct.area: Within the focal huc the percent of the area of the system that falls within the huc based cores", 
  "\thuc.core.pct.si: Within the focal huc the perecentage of the selection index of the system that is captured by the huc based cores", 
  
  "\treg.core.tot.si: The sum of the selection index within the regional cores, the focal system, and the focal huc",
  "\treg.core.mean.si: The mean of the selection index within the regional cores, the focal system, and the focal huc",
  "\treg.core.cells: The count of cells within the regional cores, the focal system, and the focal huc",
  "\treg.core.pct.area: Within the focal huc the percent of the area of the system that falls within the regional cores.",
  "\treg.core.pct.si: Within the focal huc the perecentage of the selection index of the system that is captured by the regional cores", 
  
  "\tarea.diff: The difference between reg.core.pct.area and huc.core.pct.area",
  "\tsi.diff: The difference between reg.core.pct.si and huc.core.pct.si"
)
writeLines(md, paste0(file, ".medatdata.txt"))
