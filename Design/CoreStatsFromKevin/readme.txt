These are Kevin's functions to calculate terrestrial core stats from summary tables.
They were subsequently integrated into LCAD.

Below is the text of Kevin's email describing them.


Ethan

See attached with R scripts and output files created from these two scripts. The tcores and acores are separate. Note that for acores we only had eco stats since we didn't have species before (except  brook trout). We may want to add the species stats to acores for the brook trout and loon, but only if it ends up not being too much trouble. Also, as you might expect, my scripts are hardwired for local paths and I believe are very sensitive to the exact input format and field names for the tables you generate. I recall having difficulty when you recreated some of the input tables and changed field names or such. 

Anyways, for the tcores, the script should create:

summary table pooled across all cores for the eco stats = t1CoreEcoSum.csv
summary table pooled across all cores for the species stats = t1CoreSpeciesSum.csv
summary table by core for the eco stats = t1CoreEcoStats.csv
summary table by core for the species stats = t1CoreSpeciesStats.csv
attribute table for the shapefile (one row for each core) = t1CoreStats.csv
a folder (t1CoreStats) containing a separate table for each core with the detailed eco stats (core#eco.csv) and detailed species stats (core#species.csv)

The acores script should create:

summary table pooled across all cores for the eco stats = aCoreEcoSum.csv
summary table by core for the eco stats = aCoreEcoStats.csv
attribute table for the shapefile (one row for each core) = aCoreStats.csv
a folder (aCoreStats) containing a separate table for each core with the detailed eco stats (core#eco.csv)

Let me know if you need to discuss.
Kevin