# Make binary version of rare communities
# Ethan Plunkett
# This produces a grid that has 1 where there are rare communities and NA where there aren't
#

rm(list = ls())
library("anthill")
source("X:/LCC/LCAD/code/buildtilemap.R")
input <- "X:/LCC/GIS/Work/Biotic/EOs/rarecomm_m"

binary <- "X:/LCC/GIS/Work/Biotic/EOs/rarecom_bin"

tiles <- buildtilemap(2000)

refgridinit(input)
tileinit(2000, 0)

for(i in seq_along(tiles)){
  tile <- tiles[i]
  settile(tile)
  a <- readtile(input, cacheok = TRUE)
  a$m[!is.na(a$m)] <- 1
  writetile(a, path = binary, as.integer = TRUE)
  cat("Done with ", i, " of ", length(tiles), "\n")
}


launchbatchstitch(binary, owner = "ebp", name = "stitch_rarecom_bin")

