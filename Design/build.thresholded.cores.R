source("X:/LCC/LCAD/Testing/load.LCAD.R")

source("X:/LCC/LCAD/Design/lc.stats.R")


parameterization <- "Dec16up"
parameterization <- "Dec16low"


mask = "X:/LCC/GIS/Work/Ancillary/mask/ctriver/mask_ctriver_m"  # same as extent but in grid form
cells.in.landscape <- 32052629  # from mask grid

parameterization.recognized <- FALSE
if(parameterization == "Dec16low"){
  parameterization.recognized <- TRUE
  mask = "X:/LCC/GIS/Work/Ancillary/RegionMaps/huc6_ctr_low_m"
  cells.in.landscape = 14399202
  result.dir <-"X:/LCC/GIS/Final/LCD/species/Dec16threshold_low/"
}

if(parameterization == "Dec16up"){
  parameterization.recognized <- TRUE
  mask = "X:/LCC/GIS/Work/Ancillary/RegionMaps/huc6_ctr_up_m"
  cells.in.landscape = 17653427
  result.dir <-"X:/LCC/GIS/Final/LCD/species/Dec16threshold_up/"
}


dir.create(result.dir, recursive = TRUE, showWarnings = FALSE)
core.grid <- paste(result.dir, "cores_m", sep="")


stopifnot(parameterization.recognized)


data.dir <- paste("X:/lcc/gis/Final/LCD/species/stats/quantilecuts/", parameterization, "/", sep="")
dir.create(data.dir, recursive = TRUE, showWarnings = FALSE)

tilesize <- 2000
tm <- coretiles(tilesize, mask = mask)

spp <- c( "blbw", "blpw", "nowa","woth", "amwo", "blackbear", "lowa", "mawr",
                 "glin", "eame", "moose", "rugr", "wodu", "praw")

lc.type  <- "initial"

gridpaths <- get.lc.paths(spp, lc.type )
  
lc.targets <- read.par(paste(path("par"), "lc.targets.par", sep="" ))
lc.targets <- lc.targets[lc.targets$species %in% spp, ]
lc.targets <- lc.targets[match(spp, lc.targets$species), ]  

# Define cutpoints in proportion of LC that start with the target percent above and 
# incrementally move towards 0% above the cutpoint
n <- 100
p.cutpoints <- matrix(NA, nrow=length(spp), ncol=n, dimnames=list(spp, paste("cp", 1:n, sep="")))
for(i in 1:length(spp)){
  p.cutpoints[i, ] <- seq(1- (lc.targets$target.percent[i]/100), 1, length.out = n )
}

# Translate cutpoints to a lc value
lc.cutpoints <- matrix(NA, nrow=length(spp), ncol=n, dimnames=list(spp, paste("cp", 1:n, sep="")))
for(i in 1:length(spp)){
  samp <- get.lc.sample(species = spp[i], mask = mask)
  samp <- sort(samp)
  prop <- cumsum(samp)/sum(samp)
  a <- approx(x = prop, y=samp, xout = p.cutpoints[i, ])$y
  lc.cutpoints[i, ] <- a
}

#plot(density(samp))
#abline(v=a)

refgridinit(mask)
tileinit(tilesize, 0)

calibrate <- function(tile){
  refgridinit(mask)
  tileinit(tilesize, 0)
  settile(tile)
  
  lcs <- vector(mode="list", length = length(spp))
  cat("reading grids:\n")
  for(i in 1:length(spp)){
    cat("\t", spp[i], "\n")
    lcs[[i]] <- readtile(gridpaths[i], cacheok=TRUE)
  }
  
  # Read and format mask
  m <- readtile(mask) 
  m$m <- m$m > 0
  m$m[is.na(m$m)] <- FALSE
  outside <- !m$m
  
  
  result <- numeric(ncol(lc.cutpoints))
  cat("Examining cut points\n")
  for(j in 1:ncol(lc.cutpoints)){
    cat(".")
    sel <- newtile()
    sel$m <- matrix(FALSE, nrow=sel$nrow, ncol=sel$ncol)
    for(i in 1:length(spp)){
      cut <- lc.cutpoints[i , j]
      sel$m <- sel$m | lcs[[i]]$m >= cut
    }
    sel$m[outside] <- NA
    result[j] <- sum(sel$m, na.rm=TRUE)  
  }
  save(result, tile, file=paste(data.dir, tile, ".Rdata", sep="")) 
  cat("Done with tile", tile, "\n")
}

#for(tile in tm)
#  calibrate(tile)

simple.launch(call="calibrate()", subtaskarg="tile", subtask=paste("range:", compact.range(tm)), name = "calibrate lc", owner="EBP")

stop("Wait for anthill to finish then continue.")

d <- matrix(NA, nrow=length(tm), ncol=n)

for(i in 1:length(tm)){
  tile <- tm[i]
  e <- new.env()
  load(paste(data.dir, tile, ".Rdata", sep=""), envir=e)  
  d[i, ] <- e$result    
}


target.p.land <- 0.25
p.land <- apply(d, 2, sum)/cells.in.landscape
a <- which.min(abs(p.land - target.p.land))
final.cutpoints <- lc.cutpoints[, a]


threshold <- function(tile){
  refgridinit(mask)
  tileinit(tilesize, 0)
  settile(tile)
  cores <- newtile()
  cores$m <- matrix(FALSE, nrow=cores$nrow, ncol=cores$ncol)
  cat("Reading spp\n")
  for(i in 1:length(spp)){
    cat(".")
    lc <- readtile(path = gridpaths[i], cacheok=TRUE)
    cores$m <- cores$m | lc$m >= final.cutpoints[i]
  }
  cat("\n")
  m <- readtile(mask, cacheok = TRUE)
  m$m <- !is.na(m$m) & m$m > 0 
  plot(m, u=T) 
  cores$m[!m$m] <- NA
  writetile(cores, core.grid, as.integer = TRUE)
}

simple.launch(call="threshold()", subtaskarg="tile", 
              subtask=paste("range:", compact.range(tm)), 
              name = "buildcores", owner="EBP")












