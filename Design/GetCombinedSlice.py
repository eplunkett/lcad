#GetPatchSize.py
#J. Grand, Oct. 22, 2014
# This script identifies patches of contiguous cells, assigns them a unique
# value and records a cell count for each patch in the outGrid VAT
# Edited by Ethan Plunkett Oct 23:
#       creates binary intermediate grid
#       exports patches VAT as csv

# Parameters:
#  inGrid: input grid
#  outGrid: output grid
#  neighbor rule: "FOUR" or "EIGHT"
#  zoneConnectivity: "CROSS" = cells of any value (except excluded) will be
#     grouped; or "WITHIN" = The only cells that will be grouped are cells of
#     the same value
#  excluded: background value; set non-patch cells to zero (or any number not
#     included in cores); required if zoneConnectivity = "CROSS"

# Modified 28 Oct 2014 by BWC to get slice for combined species + ecosystems.  
#Need:
#1. Change 1228 to whatever slice Ethan gives us Tuesday afternoon.  Using 1228, which gives us 26.6% of the landscape.
#2. Need to union terrestrial cores and species cores after sliceing, before regiongroup - added by JG on 10/28/14



import arcpy,sys,os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.overwriteOutput = True

spDir  = "X:/LCC/GIS/Work/LCD/species/Oct26_Comb_40k80x10ps/"
inGrid = spDir + "fullgrids/cores_s"  
rcGrid = spDir + "slice1228/bincores_s"  #change slice directory
inGrid2 = "X:/lcc/gis/work/lcd/coresr/tws97-13g"
neighborRule = "EIGHT"
zoneConnectivity = "WITHIN"
comboDir = "X:/LCC/GIS/Work/LCD/combo/weighted"
outGrid = comboDir + "/patches"
outCSV = comboDir + "/slice1228.patches.csv"  #change x to 13% slice

rc = Reclassify(inGrid, "Value" , RemapRange([[1, 1228, 1], [1229, 100000, "NoData" ]]), "DATA" ) #change 1228 and 1151 to 13% slice
rc.save(rcGrid)
print "finished slicing species cores"

arcpy.MosaicToNewRaster_management([rcGrid,inGrid2],comboDir,"combocores","#","1_BIT","30","1","MAXIMUM")
print "finished combining species and system cores"

outRG = RegionGroup(comboDir + "/combocores", neighborRule, zoneConnectivity)
outRG.save(outGrid)
print "finished creating patches"

print "exporting VAT to CSV"
lstFlds = arcpy.ListFields(outGrid)
header = ''
for fld in lstFlds:
    header += ", " + format(fld.name)
header = header[1:] # drop initial ","
if len(lstFlds) != 0:
    f = open(outCSV,'w')
    f.write(header + '\n')
    rows = arcpy.SearchCursor(outGrid)
    for row in rows:
        rowText = ""
        for field in lstFlds:
            rowText = rowText + str(row.getValue(field.name)) + ", "
        rowText = rowText[:-2]
        f.write(rowText +  '\n')
    f.close()
print "finished exporting VAT to CSV"
