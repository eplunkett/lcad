#GetCombinedSlice.py
#J. Grand, Oct. 22, 2014
# This script slices the full species core grid at a specified core number to achieve 13% of the landscape and sets all 
# core values to 1, mosaics the result with the 13% ecosystem cores, identifies patches of contiguous cells and assigns 
# them a unique id and cell count, then outputs the patch grid VAT to a csv file

# Parameters:
#  spDir: directory of species run output
#  slice: core number at which to slice full species core grid to obtain 13% of the landscape
#  inGridSp: full species core grid
#  rcGridSp: full species core grid reclassified to 1/nodata using 13% slice 
#  inGridEs: 13% ecosystem core grid
#  neighbor rule: "FOUR" or "EIGHT"
#  zoneConnectivity: "CROSS" = cells of any value (except excluded) will be grouped;
#     or "WITHIN" = The only cells that will be grouped are cells of the same value
#  excluded: background value; set non-patch cells to zero (or any number not
#     included in cores); required if zoneConnectivity = "CROSS"
#  comboDir: output directory for combo cores grid
#  outGrid: grid of unique patch ids for combo cores grid
#  outCSV: path and filename for csv file containing patch ids and cell counts

# Modified 23 Oct 2014 by EP to create binary intermediate species core grid and export patches VAT as CSV.
# Modified 28 Oct 2014 by BWC to get slice for combined species + ecosystems.  
# Modified 28 Oct 2014 by JG to mosaic sliced species cores and 13% ecosystem cores.
# Modified 07 Nov 2014 by JG to add slice parameter 

#import required modules
import arcpy,sys,os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.overwriteOutput = True

#set parameters
spDir  = "X:/LCC/GIS/Final/LCD/species/new"
slice = 1228
inGridSp = spDir + "/fullgrids/cores_s"  
rcGridSp = spDir + "/slice" + str(slice) + "/bincores_s"  
inGridEs = "X:/lcc/gis/final/lcd/ecosystems/tws97-13g"
neighborRule = "EIGHT"
zoneConnectivity = "WITHIN"
comboDir = "X:/LCC/GIS/final/LCD/combo"
outGrid = comboDir + "/patches"
outCSV = comboDir + "/slice" + str(slice) + ".patches.csv"  

#do the work
rc = Reclassify(inGridSp, "Value" , RemapRange([[1, slice, 1], [slice + 1, 100000, "NoData" ]]), "DATA" ) 
rc.save(rcGridSp)
print "finished slicing species cores"

arcpy.MosaicToNewRaster_management([rcGridSp,inGridEs],comboDir,"combocores","#","1_BIT","30","1","MAXIMUM")
print "finished combining species and system cores"

outRG = RegionGroup(comboDir + "/combocores", neighborRule, zoneConnectivity)
outRG.save(outGrid)
print "finished creating patches"

print "exporting VAT to CSV"
lstFlds = arcpy.ListFields(outGrid)
header = ''
for fld in lstFlds:
    header += ", " + format(fld.name)
header = header[1:] # drop initial ","
if len(lstFlds) != 0:
    f = open(outCSV,'w')
    f.write(header + '/n')
    rows = arcpy.SearchCursor(outGrid)
    for row in rows:
        rowText = ""
        for field in lstFlds:
            rowText = rowText + str(row.getValue(field.name)) + ", "
        rowText = rowText[:-2]
        f.write(rowText +  '/n')
    f.close()
print "finished exporting VAT to CSV"
