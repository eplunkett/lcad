These are copies of Brad Compton's APL Ecosystem core building functions as
they existed Nov 28, 2018.  

They are here to document how ecosystem cores were built for the analysis used 
in the manuscript: "Landscape conservation designs Alternative approaches to 
landscape conservation design in the northeastern United States"