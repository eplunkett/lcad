
# Make species mask
#
#  This converts the standard mask from 0 to 1 for cells that are ocean and within a phase3 species footprint.
# The whole point is to extend the mask out into the ocean in the same weird way we did with prior species.
#  Standard mask isn't great for speecies because many species have LC in the near shore zone.
#
#  I use the footprint of 1's in this grid to trim the final species results to our landcape.
#  It looks like a few of the tiles that inersect the zeros only didn't run so the zero's aren't completely
#  correct but I'm not using them.
#
#  Ethan Plunkett
# 



# Input paths
reference <- "X:/LCC/GIS/Work/Ancillary/Reference/ref_ner_m"
mask <- "X:/LCC/GIS/Work/Ancillary/Mask/phase5/mask_m" # phase5 mask 
spp <- "X:/LCC/GIS/Work/Biotic/species/LCAD2010/abdu/lc"   # phase 3 species
land <- "X:/LCC/GIS/Work/Landcover/ESMplus/ner_phase5/esmplus_es_m"

ocean <- c(201L, 400L, 402L, 500L, 502L)   # dput(lookup(c("ocean", "greatlakes")))

# Output path
spmask <- "X:/LCC/GIS/Work/Ancillary/Mask/phase5/ref_ner_spp"


.status$tiff <- FALSE  #### Probably will want to delete

mosaicinit(reference)
tm <- coretiles(2000)


tileinit(2000, 0)
for(i in seq_along(tm)){
  settile(tm[i])
  m <- readtile(mask)
  s <- readtile(spp)  
  l <- readtile(land)
  sv <- m$m == 0 & !is.na(s$m)  & l$m %in% ocean
  sv[is.na(sv)] <- FALSE
  m$m[sv] <- 1
  cat("Tile ", i, ", ", sum(sv), "cells flipped.\n")
  writetile(m, path = spmask, as.integer= TRUE)  
}

ref <- gsub("_m$", "", reference)
launchbatchstitch(spmask, reference = ref, owner = "ebp", priority = 6)
stitch(spmask, paste0(spmask, "_s"), reference = ref)
