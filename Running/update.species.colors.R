# Script to update the colors assigned to species in species.info.par


source("X:/LCC/LCAD/Code/read.par.R")
source("X:/LCC/LCAD/Code/write.par.R")
# Colors
colors <- c("#0075DB", "#2BCD48", "#740AFE", "#808080", 
            "#9CCB00", "#F781BF", "#FE0010", "#FE5005")

si.par <- "X:/LCC/LCAD/Parameters/species.info.par"

# Lty 
lty <- c("solid", "42",  "6212", "A2") 

plotcolors <- function(colors, vertical = TRUE, add = FALSE, ...) {
  n <- length(colors)
  if(vertical){
    if(!add)
      plot(x=1:n, y = rep(1,n), col = colors, ...)
    lapply(1:n, function(x) abline(v=x, col = colors[x]))
  } else {
    if(!add)
      plot(y=1:n, x = rep(1,n), col = colors, ...)
    lapply(1:n, function(x) abline(h=x, col = colors[x]))
  }
  return(invisible())
}

plotcolors(colors)

si <- read.par(si.par)
si$col <- rep(colors, each = 4, length.out = nrow(si))
si$lty  <-  rep(lty, length.out = nrow(si))

plot(NA, NA, xlim = c(1, nrow(si)), ylim = c(0, 1), ann = FALSE)
abline(v = 1:nrow(si), col = si$col, lty = si$lty, lend = 1)

write.par(si, si.par)




if(FALSE){
  # The rest of this is exploration of various color options
  
  
  # Add color  and line types to species.selection.par
  #
  # I'm going to use 3 line types and thus need 11 colors to have no duplicates.  
  #  
  
  #### Explore color palettes  
  
  plotcolors <- function(colors, vertical = TRUE, add = FALSE, ...) {
    n <- length(colors)
    if(vertical){
      if(!add)
        plot(x=1:n, y = rep(1,n), col = colors, ...)
      lapply(1:n, function(x) abline(v=x, col = colors[x]))
    } else {
      if(!add)
        plot(y=1:n, x = rep(1,n), col = colors, ...)
      lapply(1:n, function(x) abline(h=x, col = colors[x]))
    }
    return(invisible())
  }
  
  # https://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
  # Robert C. Carter, Ellen C. Carter (1982): High-contrast sets of colors. // Appl. Optics, 21 (1982) 2936-2939.
  cm <- matrix(c(240,163,255,
                 0,117,220,
                 153,63,0,
                 76,0,92,
                 25,25,25,
                 0,92,49,
                 43,206,72,
                 255,204,153,
                 128,128,128,
                 148,255,181,
                 143,124,0,
                 157,204,0,
                 194,0,136,
                 0,51,128,
                 255,164,5,
                 255,168,187,
                 66,102,0,
                 255,0,16,
                 94,241,242,
                 0,153,143,
                 224,255,102,
                 116,10,255,
                 153,0,0,
                 255,255,128,
                 255,255,0,
                 255,80,5), 
               byrow = TRUE,
               ncol = 3)
  
  colors <- apply(cm, 1, function(x) do.call(rgb, args = as.list(x/256)))
  sv <- apply(cm, 1, sum) < 500
  sv <- sv & apply(cm, 1, sum) > 200
  sum(sv)
  
  colors1 <- sort(colors[sv])
  plotcolors(colors1)
  
  
  #http://godsnotwheregodsnot.blogspot.ru/2012/09/color-distribution-methodology.html
  
  colors <-  c("#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
               "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
               "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
               "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
               "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
               "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
               "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
               "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
               "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
               "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
               "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
               "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
               "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C")
  
  colors <- sort(colors)
  
  
  plotcolors(colors)
  
  source("X:/LCC/LCAD/Code/read.par.R")
  source("X:/LCC/LCAD/Code/write.par.R")
  si <- read.par("X:/LCC/LCAD/Parameters/species.info.par")
  
  species.selection.par <- "X:/LCC/LCAD/Parameters/species.selection.par"
  
  
  lcc.cols <- unique(si$col)
  lcc.lty <- unique(si$lty)
  plotcolors(lcc.cols, vertical = TRUE, ylim = c(1, length(colors1)))
  plotcolors(colors1, vertical = FALSE, add = TRUE)
  
  select.from.lcc <- c( 2)
  select.from.1  <- c(1, 3, 4, 5, 7, 8, 10, 11 )
  
  new.colors <- sort(c(lcc.cols[select.from.lcc], colors1[select.from.1],  rgb(.1, .1, .1), "#42eef4"))
  new.colors <- c(new.colors[-7], new.colors[7])  # put this one last because I like it least 
  # (it will only be used once, the others will all be used three times)
  
  # This should be identical to the above
  colors <- c("#0075DB", "#1A1A1A", "#2BCD48", "#42eef4", "#740AFE", "#808080", 
              "#9CCB00", "#F781BF", "#FE0010", "#FE5005", "#983F00")
  
  plotcolors(new.colors, ylim = c(1, length(new.colors)))
  plotcolors(new.colors, vertical = FALSE, add = TRUE)
  
  
  a <- read.par
  
  lty <- c()
  
  
  set.seed(1)
  color.key <- data.frame(id = ids, 
                          col = rep(new.colors, each = 3, length.out = n), 
                          lty = rep(lcc.lty, times = 3, length.out = n))
  
  mv <- match(a$id, color.key$id)
  
  b <- a
  b$col <- color.key$col[mv]
  b$lty <- color.key$lty[mv]
  
  
  b <- b[, c("scenario", "species", "alias",
             "year", "col", "lty",  "comment")]
  
  write.par(b, file = species.selection.par)
  
  
  
  
  
  
  
  
  
}





