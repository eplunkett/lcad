This directory contains code to launch projects and or run analysis locally.  
On June 20, 2017 I moved these files from ../testing/ which was previously used for the same purpose.
I left older code that I didn't think would be useful in testing so if you are looking for something old check there.


Code for launching conservation design related functions (Core building, core stats etc).  Is in:
	X:\LCC\LCAD\Design    (with related functions in X:\LCC\LCAD\Design\functions\

Finally, I often have example usage before the function definition within an if(FALSE) block that can be used to run the function.  
	e.g. X:\LCC\LCAD\Code\calc.static.window.metrics.R
	
	
I don't think this directory should ever be deleted.

Ethan Plunkett
June 20, 2017

Note:  Summer 2018 I've created a new directory  ../Stats/ for scripts that are
run to calculate statistics and moved some things from here to there.

The intent is that this directory has scripts that launch projects on the cluster to run 
models.  ../stats/ may or may not launch projects but is generally summarize runs 
into tables.

