# Code to make a shapefile with the extent of each mosaic window
source("X:/LCC/LCAD/load.LCAD.R")
refgridinit(g("reference"))

d <- griddescribe(g("reference"))
pm <- d$mosaicinfo$panemap


con <- saveconnections()
con <- con$connections
con <- con[grep("z", ignore.case = TRUE, con$drive) ,  ]

pl <- vector(mode="list", length= nrow(con))
ids <- con$pane
for(i in 1:nrow(con)){
  l <- as.list(con[i, ])
  b <- getbounds(l)
  mat <- as.matrix(cbind(as.numeric(c(b[1, ], b[1, 2:1])), as.numeric(rep(b[2, ], each = 2))))
  pl[[i]]  <- Polygons(list(Polygon(mat)), ID = i)
}
con$id <- 1:nrow(con)
rownames(con) <- NULL
sp <- SpatialPolygons(pl, pO = 1:nrow(con))
sp <- SpatialPolygonsDataFrame(sp, data = con)
plot(sp)
plot.lcc.boundary()
writeshape(sp, "X:/LCC/GIS/Work/Ancillary/Boundaries/Tiles/nerMosaic.shp")
