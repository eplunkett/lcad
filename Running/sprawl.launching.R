
# This is code to more or less manually run sprawl (for testing and development)
#  See launching.commands.R for standard launching of sprawl
# Also note.
#  preprocess() is a wrapper that calculates some extent and phase specific inputs that can be used 
#   accross scenarios and reps
#  It calls  
#    make.input.spg()
#    calc.static.window.metrics()
#    make.input.traffic.kernel   # defunct (runnable but doesn't do anything and isn't required)

source("X:/LCC/LCAD/load.LCAD.R")
check.synonyms()

library(anthill)
library(gridio2)

read.parameters(check=FALSE)
set.scenario("baseline2")
set.timestep(1)

# lcad(scenario = "baseline", rep = 1, job = c("calc.window.metrics", "wait", "calc.dev.prob"),owner = "EBP", priority = 0)
     
local=FALSE
use.anthill=FALSE
owner="LCAD"
rep=2
scenario <- "baseline2"
set.rep(rep)


g("land", 0)
ref <- g("reference")
refgridinit(ref)
refdesc <- griddescribe(ref)

ct <- coretiles(tilesize=get.tilesize("sprawl"))
tile <- ct[1]
timestep <- 1

# this makes all the static grids needed for sprawl prediction - should just be run once for new input data
if(FALSE)
  make.input.spg()

# Calculate the static window metrics
#
if(FALSE){  # note this doesn't run in parallel so could be sped up if need be.  It only runs once per landscape
  simple.launch(call = 'calc.static.window.metrics(scenario=scenario)', name ="winmetrics", owner = "ebp")  
}
#  calc.static.window.metrics(scenario=scenario)  # local


# Calculate step 1 window metrics - this is the first part of the iterative sprawl process
# I'm running it more or less manually here for code development

do.one <- function(tile, timestep){
  refgridinit(g("reference"))
  calc.window.metrics(tile = tile, timestep = timestep)
} 

do.one <- function(tile, timestep){
  refgridinit(g("reference"))
  calc.dev.prob(tile = tile, timestep = timestep)
} 

do.one <- function(tile, timestep){
  refgridinit(g("reference"))
  normalize.dev.prob.a(tile = tile)
} 


do.one <- function(tile, timestep){
  refgridinit(g("reference"))
  normalize.dev.prob.b(tile = tile)
} 



do.one <- function(tile, timestep){
  refgridinit(g("reference"))
  develop(tile = tile, timestep = timestep)
} 


if(FALSE)
  simple.launch(call="do.one(timestep=timestep)", subtaskarg="tile", subtask=paste("range:", compact.range(ct)), owner="EBP",
              name="develop", maxthreads=2, priority =5)



apply <- function(tile, timestep){
  library(gridio2)
  refgridinit(g("reference"))
  apply.sprawl(tile = tile, timestep = timestep)
} 

apply.ct <- coretiles(tilesize=get.tilesize("apply.sprawl"), buffersize = 0)

if(FALSE)
  simple.launch(call="apply(timestep=timestep)", subtaskarg="tile", subtask= make.range(values = apply.ct), owner="EBP",
                name="applysprawl", maxthreads=10, priority =5)


mosaics <- c(g("sprawl", t = 1), g("land", t = 1))
launchbatchstitch(mosaics, owner = "EBP")



step1 <- function(tile, timestep){
  refgridinit(g("reference"))
  calc.window.metrics(tile, timestep)
  calc.dev.prob(tile=tile, timestep=timestep)
}

simple.launch(call="step1(timestep=timestep)", subtaskarg="tile", subtask=paste("range:", compact.range(ct)), owner="EBP",
              name="Sprawlin", maxthreads=5)



set.maxthreads("Sprawlin", 20)

allocate.sprawl(timestep=1)


for(tile in ct){
  develop(tile, 1)
}


dev <- function(tile, timestep){
  refgridinit(g("reference"))
  develop(tile = tile, timestep = timestep)
}
timestep <- 1

ct2 <- coretiles(tilesize = get.tilesize("apply.sprawl"), buffersize = 0)
a.sprawl <- function(tile, timestep){
  refgridinit(g("reference"))
  apply.sprawl(tile = tile, timestep = timestep)
}

simple.launch(call="a.sprawl(timestep=timestep)", subtaskarg = "tile",
              subtask = paste("range:", compact.range(ct2)), priority=0, maxthreads=20,
              owner="EBP", name="applysprawl")


