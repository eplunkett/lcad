# 
# This script is for analyzing the sprawl algorythm
# It depends on data structure produced during the sprawl run as well as the repackaged 
#   sprawl fit objects

# The bulk of what used to be here is now in map.panes() and what remainds are the dregs.


# Ethan PLunkett 


#####
# Note function seems to be missing - skipping
if(FALSE){
  rd <- assemble.realized.stats(scenario=scenario, timestep=timestep)
  
  d <- merge(allocation.stats, rd, by.x="pane.id", by.y="id", all=TRUE)
  d$total.realized <- apply(d[, names(d) %in% 1:6], 1, sum)
  d$total.new <- apply(d[, names(d) %in% 1:3], 1, sum)
  lim <- c(0, 1000)
  plot(d$total.cells.needed, d$total.new, xlim=lim, ylim=lim)
  abline(a=0, b=1, col="red")
  
  #sv <- names(d)  %in% 1:6
  #names(d)[sv] <- paste("t", names(d)[sv], sep="")
  
  e <- SpatialPolygonsDataFrame(a, data=d, match.ID="pane.id")
  
  writeshape(e, file=paste(g("output"), "/Anthropogenic/realized", timestep, ".shp", sep=""))
}


  
  
  ##### Tabulate Realized Development #####
  new.development <- matrix(nrow=7, ncol=3)
  redevelopment <- matrix(nrow=7, ncol=3)
  rownames(new.development) <- row.names(redevelopment) <- paste("TS", 1:7, sep="")
  colnames(new.development) <- colnames(redevelopment) <- paste("rep", 1:3, sep="")
  # for(rep in 1:3){
  # for(step in 1:7){
  # Read in realized transition data
  setwd(paste("X:/LCC/GIS/Output/",scenario, "/", rep, "/windowstats/realized", sep=""))
  l <- list.files()
  l <- grep( paste("step", timestep, "[.]tile", sep=""), l, value=TRUE)
  ntrans <- 6
  chunk.size <- 100
  
  developed.cells <- matrix(nrow=chunk.size, ncol=ntrans)
  max.rows <- chunk.size
  ri <- 1
  for(i in 1:length(l)){
    n <- l[i]
    load(n)
    for(j in 1:length(realized.stats)){
      if(is.null(realized.stats[[j]])) next
      developed.cells[ri, ] <- realized.stats[[j]]$developed.cells
      ri <- ri + 1
      if(ri > max.rows){
        developed.cells <- rbind(developed.cells, matrix(nrow=chunk.size, ncol=ntrans))
        max.rows <- max.rows + chunk.size
      }
    }
  }  
  developed.cells <- developed.cells[1:(ri-1), ]
  
  a <- apply(developed.cells, 2, sum)
  new.development[timestep, rep] <- sum(a[1:3])
  redevelopment[timestep, rep] <- sum(a[4:6])
  # } # temporary end to loop through steps
  # } # temporaryend to loop through reps
  
  
  
  
  
  
  ##### Tabulate Allocated  Development (Total in Region) #####
  # Read in allocation data
  setwd(paste(g("output"), "/windowstats/allocation", sep=""))
  load(paste("step", timestep,".alltiles.Rdata",sep=""))
  
  sum(allocation.stats$total.cells.needed, na.rm=TRUE)
  sum(allocation.stats[, grep("t[123]\\.cells\\.needed", names(allocation.stats))], na.rm=TRUE)
  sum(allocation.stats[, grep("t[123456]\\.cells\\.needed", names(allocation.stats))], na.rm=TRUE)
  sum(allocation.stats$proportion)
  
  
  load.sprawl.fit()  # load("X:/LCC/LCAD/Parameters/sprawl/fits/all02.Rdata")
  names(windows.meta.data)[names(windows.meta.data) == "universal.id"] <- "window.id"
  a <- allocation.stats
  for(i in 1:3){
    a <- merge(a, windows.meta.data[,c("window.id", "region")], by.x=paste("match", i, "id", sep=""), by.y="window.id")
    names(a)[names(a) %in% "region"] <- paste("match", i, "region", sep="")
  }
  
  
  for(i in 1:3) print(table(a[[paste("match", i,  "region", sep="")]]))
  
  b <- a[, grep("match[123]region", names(a))]
  c <- apply(b, 1, function(x) all(x[1] == x))
  cat(round(sum(c)/length(c)*100, 1), 
      "% of application windows matched to only one region")
  
  
  d <- a[, c("tile.id", "pane.id",  grep("match[123]region", names(a), value=TRUE))]
  
  red <- apply(b,1,  function(x) sum(x =="ches")/length(x))
  blue <- apply(b,1,  function(x) sum(x =="mass")/length(x))
  green <- apply(b,1,  function(x) sum(x =="maine")/length(x))
  cols <- rgb(red=red, green=green, blue=blue)
  
  d <- cbind(d, red, blue, green, cols)
  spolys <-   SpatialPolygons(polys, pO=1:length(polys))
  
  head(allocation.stats)
  
  spd <- SpatialPolygonsDataFrame(spolys, data=d, match.ID="pane.id")
  
  writeshape(, file=paste(g("output"), "/Anthropogenic/allocation", timestep, ".shp", sep=""), overwrite_layer=TRUE)
  
  
  ##### Define Functions  ####
  open.plot <- function(filename){
    png(filename=filename,width=1000, height=1000, units="px")
  }
  close.plot <- function() dev.off()
  
  
  
  # Make Plots
  setwd(g("output"))  # plots will be saved here
  
  #### Plot  regions of match (color gradiation) ####
  open.plot(paste("region.of.match.ts.", timestep, ".png", sep=""))
  red <- apply(b,1,  function(x) sum(x =="ches")/length(x))
  blue <- apply(b,1,  function(x) sum(x =="mass")/length(x))
  green <- apply(b,1,  function(x) sum(x =="maine")/length(x))
  cols <- rgb(red=red, green=green, blue=blue)
  plotextent()
  title(main="Region of match")
  mtext(paste(scenario, " rep=", rep))
  for(i in 1:ntiles()){
    cat(".")
    settile(i)
    for(j in 1:nsubtiles()){   
      setsubtile(j)
      id <- stid()
      if(id %in% d$pane.id){
        st <- subtiledetails(coreonly=TRUE)
        row <- which(d$pane.id %in% id)[1]
        col <- cols[row]
        
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        polygon(x=c(rep(xmin, 2), rep(xmax, 2)), y=c(ymin, ymax, ymax, ymin), col=col)
      }
    }
  }
  cat("\n")
  close.plot()
  
  # Plot dominant region of match
  fun <- function(x){
    t <- table(x)
    names(t)[which.max(t)]
  }
  d$dominant.region <- apply(b, 1, fun)
  plotextent()
  for(i in 1:ntiles()){
    cat(".")
    settile(i)
    for(j in 1:nsubtiles()){   
      setsubtile(j)
      id <- stid()
      if(id %in% d$pane.id){
        st <- subtiledetails(coreonly=TRUE)
        row <- which(d$pane.id %in% id)[1]
        col <- switch(d$dominant.region[row], 
                      "ches"="red", 
                      "maine"= "green",
                      "mass"= "blue"
        )
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        polygon(x=c(rep(xmin, 2), rep(xmax, 2)), y=c(ymin, ymax, ymax, ymin), col=col)
      }
    }
  }
  cat("\n")
  
  
  # Plot match proportion of match
  
  e <- (allocation.stats$proportion)
  e <- e- min(e)
  e <- e / max(e)
  e <- 1- e
  cols <- grey(e)
  
  open.plot(paste("growth.allocation.ts.", timestep, ".png", sep=""))
  plotextent()
  title(main="Allocation of new developtment")
  mtext(paste(scenario, " rep=", rep, " step=", timestep, sep=""))
  for(i in 1:ntiles()){
    cat(".")
    settile(i)
    tile <- newtile()
    for(j in 1:nsubtiles()){   
      setsubtile(j)
      id <- stid()
      if(id %in% d$pane.id){
        st <- extractsubtile(tile, coreonly=TRUE)
        row <- which(allocation.stats$pane.id %in% id)[1]
        col <- cols[row]
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        polygon(x=c(rep(xmin, 2), rep(xmax, 2)), y=c(ymin, ymax, ymax, ymin), col=col)
        
      }
    }
  }
  cat("\n")
  close.plot()
  
  # Plot built proportion
  
  e <- (allocation.stats$built.proportion)
  e <- e- min(e)
  e <- e / max(e)
  e <- 1- e
  cols <- grey(e)
  
  open.plot(paste("built.proportion", timestep, ".png", sep=""))
  plotextent()
  for(i in 1:ntiles()){
    cat(".")
    settile(i)
    tile <- newtile()
    for(j in 1:nsubtiles()){   
      setsubtile(j)
      id <- stid()
      if(id %in% d$pane.id){
        st <- extractsubtile(tile, coreonly=TRUE)
        row <- which(allocation.stats$pane.id %in% id)[1]
        col <- cols[row]
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        polygon(x=c(rep(xmin, 2), rep(xmax, 2)), y=c(ymin, ymax, ymax, ymin), col=col)
        
      }
    }
  }
  cat("\n")
  title(main="Built Proportion")
  mtext(paste(scenario, " rep=", rep, "step=", timestep, sep=""))
  close.plot()
  
  # Plot buildable proportion
  open.plot(paste("buildable.proportion.ts.", timestep, ".png", sep=""))
  e <- (allocation.stats$buildable.proportion)
  e <- e- min(e)
  e <- e / max(e)
  e <- 1- e
  cols <- grey(e)
  
  plotextent()
  for(i in 1:ntiles()){
    cat(".")
    settile(i)
    tile <- newtile()
    for(j in 1:nsubtiles()){   
      setsubtile(j)
      id <- stid()
      if(id %in% d$pane.id){
        st <- extractsubtile(tile, coreonly=TRUE)
        row <- which(allocation.stats$pane.id %in% id)[1]
        col <- cols[row]
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        polygon(x=c(rep(xmin, 2), rep(xmax, 2)), y=c(ymin, ymax, ymax, ymin), col=col)
        
      }
    }
  }
  cat("\n")
  title(main="Buildable Proportion")
  mtext(paste(scenario, " rep=", rep, " step=", timestep, sep=""))
  close.plot()
  
} # ENd for each timestep
