# Summarize species impact
#
source("X:/LCC/LCAD/load.LCAD.R")


scenarios <- c("baseline", "species", "sprefugia", "eco",  "ecorefugia", "geo",
                "union",  "compmean", "compmax", "rcoa")  #  "plusBoth",  - don't want to do cleanup on plusboth

excluded.species <- "eame"

scenario.long.names <- c(
  "Baseline",
  "Species landscape capability",
  "Species climate refugia",
  "Ecosystem integrity",
  "Ecosystem climate refugia",
  "Geophysical",
  "Union",
  "Complement (mean)",
  "Complement (max)",
  "Nature's Network"
)

reps <- 1
rep.tab <- expand.grid(list(scenario = scenarios, rep = reps) )

set.timestep(7)

# Make list of species ready to run
si <- p("species.info")
si$qc.complete <- as.logical(si$qc.complete)
spp <- setdiff(si$species[si$qc.complete], "dev")

get.stats.file <- function(scenario, rep, species){
  set.scenario(scenario, quiet = TRUE)
  set.rep(rep)
  return(paste0(g("species_stats", species = species, check.for.mosaic = FALSE), ".Rds"))
}

# Make matrix of stats files
stats.files  <- matrix(NA_character_,nrow = nrow(rep.tab), ncol = length(spp), dimnames = list(scenario = rep.tab$scenario, species = spp))
for(i in seq_along(spp)){
  stats.files[,  i] <-  mapply(get.stats.file, scenario = rep.tab$scenario, rep = rep.tab$rep, species = spp[i])
}



initial.lc <- final.lc <- species.impact  <- matrix(NA,nrow = nrow(rep.tab), ncol = length(spp), 
                                                     dimnames = list(scenario = rep.tab$scenario, species = spp))



# Note: During postprocessing I switched back and forth between Bill's and LCAD's LC grids
# Thus I don't trust the delta.lc or initial LC in the cached files. 
# I'm recalculating initial lc here.
statsdir <- "X:/LCC/GIS/Future/speciesSummary/baseline2010/" 
dir.create(statsdir, showWarnings = FALSE)
set.scenario("baseline")
set.timestep(0)

initial.lc.paths <- sapply(spp, function(x) g("lc", species = x))
initial.lc.stats.files <- paste0(statsdir, p("species.info")$species, "_2010LCstats.Rda")
tilesize <- 2000
tileinit(tilesize, 0)
ct <- coretiles(tilesize, 0)
con <- saveconnections()
gi <- savegridinfo()


tot.one <- function(i){
  restoreconnections(con)
  restoregridinfo(gi)
  tot <- 0
  for(tile in ct){
    cat(".")
    settile(tile)
    t <- readtile(path = initial.lc.paths[i], cacheok = TRUE)
    tot <- sum(tot, t$m, na.rm = TRUE)
  }
  saveRDS(tot, initial.lc.stats.files[i])
}

simple.launch(call = "tot.one()", subtaskarg = "i",
              subtask = make.range(end = length(spp), scramble = FALSE),
              name = "sum2010lc",
              owner = "ebp",
              maxthreads = 15
              )



for(i in 1:nrow(initial.lc))for(j in 1:ncol(initial.lc)){
  ilc <- readRDS(initial.lc.stats.files[j])
  a <- readRDS(stats.files[i, j])  
 # stopifnot(a$total.lc0 == ilc)
  # initial.lc[i, j] <- a$total.lc0  
  initial.lc[i, j] <- ilc
  final.lc[i, j] <- a$total.lc
  species.impact[i, j] <- a$total.lc - ilc
}

# Calculate pct of LC for each species and design scenario
pct.lc <- round((final.lc - initial.lc )/initial.lc * 100, 2)


calc.rel.impact <- function(x) x/ x[1]

relative.impact <- apply(species.impact, 2, calc.rel.impact)
relative.impact <- round(relative.impact*100, 1)


# NOTE excluding excluded.species from mean!
mean.impact <- apply(pct.lc[ , !colnames(pct.lc) %in% excluded.species], 1, mean)
summary <- data.frame(mean.impact, relative = mean.impact / mean.impact[1]* 100)
summary <- round(summary, 1)
summary <- data.frame(internal.name = rownames(summary), design = scenario.long.names,  summary)
rownames(summary) <- NULL


# Format and expand pct.lc into final table
pct.lc <- t(pct.lc)
pct.lc  <- as.data.frame(pct.lc)
pct.lc1 <- round(pct.lc, 1)
m <- apply(pct.lc, 1, mean)
cv <- apply(pct.lc, 1, FUN = function(x) sd(x)/abs(mean(x)) * 100)
pct.lc <- round(pct.lc, 1)


pct.lc <- data.frame(alias = rownames(pct.lc), 
                     Species = si$name[match(rownames(pct.lc), si$species)], 
                     pct.lc)

# Drop excluded species
# pct.lc <- pct.lc[!pct.lc$alias %in% excluded.species, , drop = FALSE]


pct.lc$`Mean (all scenarios)` = round(m, 1)
pct.lc$`Coefficient of variation (all scenarios)` = round(cv, 1)
pct.lc <- pct.lc[order(pct.lc$Species), ]
rownames(pct.lc) <- NULL  

mv <- match(names(pct.lc), scenarios)
sv <- !is.na(mv)
names(pct.lc)[sv] <- scenario.long.names[mv[sv]]




write.csv(pct.lc, "X:/LCC/GIS/Future/speciesSummary/pct.change.in.lc.csv", row.names = FALSE)
write.csv(summary, "X:/LCC/GIS/Future/speciesSummary/mean.scenario.pct.change.csv", row.names = FALSE)

write.csv(final.lc, "X:/LCC/GIS/Future/speciesSummary/2080.lc.csv")
write.csv(initial.lc, "X:/LCC/GIS/Future/speciesSummary/2010.lc.csv")
# write.csv(species.impact, "X:/LCC/GIS/Future/speciesSummary/tot.species.impact.csv")






